import { Route, Routes } from "react-router-dom"
import PersistentDrawerLeft from "../Components/adminPageComponent/pageComponent/drawer";
import routes, { adminRouterList } from "../routes";

const Content = () =>{
    return (
        <>
        <Routes>
            {routes.map((router, index)=>{
                if(router.path) {
                    return <Route key = {index}  exact path={router.path} element={router.element}>

                    </Route>
                }
            })}
            {adminRouterList.map((router, index)=>{
                if(router.path) {
                    return <Route key = {index}  exact path={router.path} element={<PersistentDrawerLeft>{router.element}</PersistentDrawerLeft>}>

                    </Route>
                }
            })}
        </Routes>
        </>
    )
}
export default Content;