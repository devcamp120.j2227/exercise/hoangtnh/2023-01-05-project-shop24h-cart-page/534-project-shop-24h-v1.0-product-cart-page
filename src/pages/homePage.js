import AboutUs from "../Components/bodyComponent/bodyContent/aboutUs"
import SliderCaroul from "../Components/bodyComponent/bodyContent/Casouline"
import LastestProduct from "../Components/bodyComponent/bodyContent/LastestProduct"

const HomePageShop24h = () =>{

    return (
    <div style ={{overflowX: "hidden"}}>
            <SliderCaroul/>
        <div className="div-lastestProduct">
            <LastestProduct/>
            <AboutUs/>
        </div>
    </div>
    )
}
export default HomePageShop24h;