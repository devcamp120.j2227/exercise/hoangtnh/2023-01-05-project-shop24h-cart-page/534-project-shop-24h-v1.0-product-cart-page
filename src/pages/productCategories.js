
import { useNavigate } from "react-router-dom";
import Categories from "../Components/bodyComponent/bodyContent/Categories";
const CategoriesProduct = () =>{
    const navigate = useNavigate();
    navigate("/Categories")
    return (
        <>
            <Categories/>
        </>
    )
}
export default CategoriesProduct;