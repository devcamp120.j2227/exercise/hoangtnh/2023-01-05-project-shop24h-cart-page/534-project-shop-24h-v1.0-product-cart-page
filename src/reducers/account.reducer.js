import {
    ALERT_STATUS, BUTTON_DELETE_HANDLER, CHANGE_STATUS_DELETE, CHANGE_STATUS_EDIT, CONFIRM_DELETE_ORDER_HANDLER,
    CUSTOMER_RESULT, FILTER_ORDER_BY_PHONE, FILTER_ORDER_BY_STATUS, GET_ACCOUNT, GET_ALL_ORDERS, GET_CURRENT_PAGE,
    GET_LENGTH, GET_ORDER_INFO, GET_PHONE_NUMBER_SEARCH, GET_SHORT_ID, GET_STATUS_ORDER, HANDLE_CHANGE_STATUS_ORDER,
    LOG_OUT, DATE_FILTER, STATUS_ORDER, SUCCESS_LOGIN, WRONG_PASSWORD, WRONG_USERNAME, GET_DATE, GET_CUSTOMER_DETAIL,
    GET_ALL_PRODUCT, GET_LENGTH_PRODUCT, GET_NUMBER_PRODUCT_STATUS, GET_CURRENT_PAGE_PRODUCT, FILTER_PRODUCT, 
    GET_PRODUCT_INFO, OPEN_TOAST, EDIT_PRODUCT_API, GET_STATUS_DELETE_IMG, GET_IMG_URL, FILTER_BY_NAME_PRODUCT, 
    GET_QUANTITY_PRODUCT, GET_CART_BAG, REMOVE_PRODUCT_IN_CART, CREATE_NEW_ORDER, GET_ORDER_RESULT, LOG_IN_ACCOUNT, LOG_OUT_ACCOUNT
} from "../constants/account.const";
const localRefToken = localStorage.getItem("refToken") || "";
const admin = JSON.parse(localStorage.getItem("admin")) || [];
const accountUser = JSON.parse(localStorage.getItem("userAccount")) || null;
const initialState = {
    account: admin.data,
    wrongAccount: "",
    openAlert: false,
    refToken: localRefToken,
    status: "",
    orders: [],
    length: 0,
    orderDetails: [],
    orderCode: [],
    orderPrice: 0,
    currentPage: 1,
    limit: 5,
    orderStatus: "All Status",
    statusEdit: false,
    statusOrder: "",
    orderInfor: {},
    numberProduct: 0,
    toast: {
        open: false,
        message: "",
        type: "success"
    },
    statusDelete: false,
    orderDelete: {},
    phoneSearch: "",
    orderResult: {},
    customer: [],
    date: [
        {
          startDate: null,
          endDate: null,
          key: 'selection'
        }],
    customerDetail: {},

    allProducts: [],
    lengthProduct: 0,
    limitProduct: 5,
    currentPageProduct: 1,
    noPage: 0,
    descpitionProduct: [
        {
            value: 'All',
            label: 'All',
          },
        {
          value: 'LivingRoom',
          label: 'LivingRoom',
        },
        {
          value: 'BedRoom',
          label: 'BedRoom',
        },
        {
          value: 'Kitchen',
          label: 'Kitchen',
        },
        {
          value: 'LoftRoom',
          label: 'LoftRoom',
        },
    ],
    productFilter: "All",
    productInfo: {},
    imgStatus: false,
    imgUrl:"",

    productName:"",
    quantity:0,
    cartBag:[],
    cartCheck: [],
    newOrder: null,

    loginWithGG: false,
    accountUser: accountUser.data,
    loginAsCustomer: "",
}

export default function accountReducer(state = initialState, action) {
    switch (action.type) {
        case GET_ACCOUNT:
            state.account = action.payload;
            break;
        case WRONG_USERNAME:
            state.wrongAccount = action.payload;
            break;
        case WRONG_PASSWORD:
            state.wrongAccount = action.payload;
            break;
        case ALERT_STATUS:
            state.openAlert = action.payload;
            break;
        case SUCCESS_LOGIN:
            state.wrongAccount = "";
            state.account = action.payload;
            state.refToken = action.payload.message;
            break;
        case LOG_OUT:
            state.refToken = action.payload;
            break;
        case GET_ALL_ORDERS:
            state.orders = action.payload;
            state.orderCode = action.payload;
            break;
        case GET_LENGTH:
            state.length = action.payload.length;
            state.noPage = Math.ceil(state.length / state.limit)
            break;
        //tìm order bằng id
        case GET_SHORT_ID:
            state.length = action.payload.length;
            state.orders = action.payload;
            state.orderStatus ="None";
            state.noPage = 1;
            state.date = [{
                startDate: null,
                endDate: null,
                key: 'selection'
            }];
            break;
        case GET_CURRENT_PAGE:
            state.currentPage = action.payload;
            break;
        //tìm order bằng status
        case FILTER_ORDER_BY_STATUS:
            if(state.orderStatus !== "All Status") {
                state.limit = action.payload.length;
                state.noPage = 1;
                state.orders = action.payload;
                state.date = [{
                                startDate: null,
                                endDate: null,
                                key: 'selection'
                            }];
                state.currentPage = 1;
                break;
            }
            if(state.orderStatus === "All Status"){
                state.date = [{
                    startDate: null,
                    endDate: null,
                    key: 'selection'
                }];
                break;
            }
            break;
        case STATUS_ORDER:
            state.orderStatus = action.payload;
            state.limit = 5;
            state.date = [{
                startDate: null,
                endDate: null,
                key: 'selection'
            }];
            break;
        case CHANGE_STATUS_EDIT:
            state.statusEdit = action.payload;
            break;
        case GET_ORDER_INFO:
            state.orderInfor = action.payload;
            break;
        case GET_STATUS_ORDER:
            state.statusOrder = action.payload;
            break;
        case HANDLE_CHANGE_STATUS_ORDER:
            state.statusOrder = action.payload;
            state.statusEdit = false;
            state.toast = {
                open: true,
                message: "Update Status Order Successfully",
                type: "success"
            }
            break;
        //khi nút delete order được bấm trên table
        case BUTTON_DELETE_HANDLER:
            state.orderDelete = action.data;
            state.statusDelete = true;
            break;

        //khi bấm đóng trên modal delete
        case CHANGE_STATUS_DELETE:
            state.statusDelete = action.data;
            break;

        //Khi bấm xác nhận xóa trên modal delete
        case CONFIRM_DELETE_ORDER_HANDLER:
            state.statusDelete = false;
            state.toast = {
                open: "true",
                message: "Delete Order Successfully",
                type: "warning"
            }
            break;
        //search by phone number
        case FILTER_ORDER_BY_PHONE:
            //state.length = action.payload.length; //chưa sử dụng vì chưa phân trang khi filter bằng số điện thoại
            state.orders = action.payload;
            state.length = 1;
            state.limit = 1
            state.date = [{
                startDate: null,
                endDate: null,
                key: 'selection'
            }];
            state.orderStatus = "None"
            break;
        case GET_PHONE_NUMBER_SEARCH:
            state.phoneSearch = action.payload;
            state.orderStatus = "None";
            state.noPage = 1
            state.date = [{
                startDate: null,
                endDate: null,
                key: 'selection'
            }];
            if (state.phoneSearch === "") {
                state.limit = 5;
                state.orderStatus = "All Status"
                break;
            }
            break;
        //search by date
        case DATE_FILTER:
            state.orders = action.payload;
            state.limit = 5
            break;
        case GET_DATE:
            state.date = action.payload;
            state.orderStatus = "None";
            if (state.date === [{
                startDate: null,
                endDate: null,
                key: 'selection'
            }]) {
                state.limit = 5;
                state.orderStatus = "All Status"
                break;
            }
            break;
        case CUSTOMER_RESULT:
            state.customer = action.payload;
            break;
        //get customer detail 
        case GET_CUSTOMER_DETAIL:
            state.customerDetail = action.payload;
            break;
        //get all product
        case GET_LENGTH_PRODUCT:
            state.lengthProduct = action.payload;
            state.noPage = Math.ceil(state.lengthProduct / state.limitProduct);
            break;
        case GET_NUMBER_PRODUCT_STATUS:
            state.numberProduct = action.payload;
            break;
        case GET_CURRENT_PAGE_PRODUCT:
            state.currentPageProduct = action.payload;
            break;
        case GET_ALL_PRODUCT:
            state.allProducts = action.payload;
            break;
        case FILTER_PRODUCT:
            state.currentPageProduct = 1
            state.productFilter = action.payload;
            break;
        case GET_PRODUCT_INFO:
            state.productInfo = action.payload;
            break;
        case OPEN_TOAST:
            state.toast = action.payload;
            break;
        case EDIT_PRODUCT_API:
            state.productInfo = action.payload;
            state.toast ={
                open: "true",
                message: "Update Product Successfully",
                type: "success"
            }
            break;
        case GET_STATUS_DELETE_IMG:
            state.imgStatus = action.payload;
            break;
        case GET_IMG_URL:
            state.imgUrl = action.payload;
            break;
        case FILTER_BY_NAME_PRODUCT:
            state.productName = action.payload;
            break;
        case GET_QUANTITY_PRODUCT:
            state.quantity = action.payload;
            break;
        case GET_CART_BAG:
            state.cartCheck.push(action.payload);
            //lấy ra toàn bộ id để kiểm tra có trùng hay không
            const ids = state.cartCheck.map(value => value.Product._id);
            //lọc những id trùng sau đó giữ lại 1 cái duy nhất
            const filtered = state.cartCheck.filter(({Product}, index) => !ids.includes(Product._id, index + 1));
            state.cartBag = filtered
            state.quantity= 0;
            state.toast = {
                open: "true",
                message: "Add Product To Cart Successfully",
                type: "success"
            }
            break;
        case REMOVE_PRODUCT_IN_CART:
            const indexRemove = state.cartCheck.findIndex(cardBag => cardBag.Product._id === action.payload);
            state.cartCheck.splice(indexRemove, 1);
            state.cartBag.splice(indexRemove, 1);
            break;
        case CREATE_NEW_ORDER:
            state.newOrder = action.payload;
            state.toast = {
                open: "true",
                message: "Create New Order Successfully",
                type: "success"
            }
            state.cartBag = [];
            state.cartCheck = [];
            break;
        case GET_ORDER_RESULT:
            state.orderResult = action.payload;
            break;
        case LOG_IN_ACCOUNT:
            state.loginAsCustomer = action.payload.message;
            state.accountUser = action.payload.data
            break;
        case LOG_OUT_ACCOUNT: 
            state.accountUser = action.payload;
            break;
        default:
            break;
    }
    console.log(state)
    return { ...state }
}
