import { GET_LENGTH } from "../constants/account.const";
import { ADD_PRODUCT_TO_CART, CHECK_CART_STATUS, CHECK_ORDER_STATUS, CONTINUE_SHOPPING_CLICK, CREATE_ORDER_OF_CUSTOMER, DELETE_PRODUCT_IN_CART, EDIT_MINUS_QUANTITY_PRODUCT, EDIT_PLUS_QUANTITY_PRODUCT, FETCH_API, GET_ADDRESS, GET_ADDRESS_DETAIL, GET_CONDITION_API, GET_DISTRICT, GET_PROFILE, GET_PROVIDE, GET_WARD, LOGIN_USER, PAYMENT_METHOD, PRODUCT_CLICK_HANDLER, QUANTITY_PRODUCT_HANDLER, SET_BIRTHDAY, SET_GENDER, SET_NAME, SET_PAGE, SET_PHONE } from "../constants/product.const";
const item = JSON.parse(localStorage.getItem("item")) || [];
const profileUser = JSON.parse(localStorage.getItem("profile")) || [];

const initialState = {
    condition: null,
    status: false,
    productDetail: null,
    quantity: 0,
    cartBag: item,
    numberProduct: item.length,
    user: null,
    checkCartStatus: false,
    profile: profileUser,
    loadProfile: {},
    nameLoad: "",
    phoneLoad:"",
    genderLoad:"",
    birthdayLoad: "",
    address: [],
    provide: [],
    selectedProvide: "",
    district: [],
    selectedDistrict: "",
    ward: [],
    selectedWard: "",
    selectedAddress: "",
    page:"profile",
    payment: "",
    newOrder: null,
    orderStatus: [],
    toastAddCart: {
        open: "false",
        message: "",
        type: "success"
    },
    length: 0
}
export default function productReducer(state = initialState, action){
        switch (action.type) {
            case GET_CONDITION_API:
                if(action.payload.description !=="All"){
                    state.condition = action.payload;
                    state.status = true;
                    break;
                }
                if(action.payload.description ==="All"){
                    state.status = false;
                    break;
                }
                break;
            case PRODUCT_CLICK_HANDLER:
                state.productDetail = action.payload;
                state.status = false;
                state.quantity = 0;
                break;
            case QUANTITY_PRODUCT_HANDLER:
                state.quantity =  action.payload;
                if(state.quantity < 0){
                    state.quantity = 0;
                    break;
                }
                break;
            case LOGIN_USER:
                state.user = action.payload;
                break;
            case ADD_PRODUCT_TO_CART:
                state.cartBag = action.payload
                state.numberProduct = action.payload.length
                // state.toastAddCart = {
                //     open: true,
                //     message: "Add Product Successfully",
                //     type: "success"
                // }
                break;
            
            case CHECK_CART_STATUS:
                state.checkCartStatus = !state.checkCartStatus;
                break;

            //khi nút tăng số lượng ấn truyền id của sản phẩm sang sau đó thay đổi quantity của sản phẩm tương ứng trong localStorage
            case EDIT_PLUS_QUANTITY_PRODUCT:
                const indexPlus = state.cartBag.findIndex(cardBag => cardBag.Product._id === action.payload)
                state.cartBag[indexPlus].quantity = state.cartBag[indexPlus].quantity + 1;
                localStorage.setItem("item",JSON.stringify(state.cartBag));
                break;

            //khi nút giảm số lượng ấn truyền id của sản phẩm sang sau đó thay đổi quantity của sản phẩm tương ứng trong localStorage
            case EDIT_MINUS_QUANTITY_PRODUCT:
                const indexMinus = state.cartBag.findIndex(cardBag => cardBag.Product._id === action.payload)
                state.cartBag[indexMinus].quantity = state.cartBag[indexMinus].quantity - 1;
                if(state.cartBag[indexMinus].quantity <= 0){
                    state.cartBag[indexMinus].quantity = 1
                }
                localStorage.setItem("item",JSON.stringify(state.cartBag));
                break;
            //khi xóa sản phẩm tìm theo id của sản phẩm sau đó cắt vị trí cần xóa tương ứng với id của sản phẩm
            case DELETE_PRODUCT_IN_CART:
                const indexRemove = state.cartBag.findIndex(cardBag => cardBag.Product._id === action.payload);
                state.cartBag.splice(indexRemove, 1);
                localStorage.setItem("item",JSON.stringify(state.cartBag));
                state.numberProduct = state.cartBag.length;
                break;
            //lấy thông tin user sau đó đẩy lên local storage
            case GET_PROFILE:
                state.loadProfile = action.payload
                    if(state.loadProfile.name !== ""){
                        state.nameLoad = state.loadProfile.Name;
                    }
                    if(state.loadProfile.gender !== ""){
                        state.genderLoad = state.loadProfile.Gender;
                    }
                    if(state.loadProfile.phone !== ""){
                        state.phoneLoad = state.loadProfile.Phone;
                    }
                    if(state.loadProfile.birthday !== ""){
                        state.birthdayLoad = state.loadProfile.Birthday;
                    }
                break;
            case FETCH_API:
                state.loadProfile = action.payload.data;
                    if(action.payload.data.Name !== ""){
                        state.nameLoad = action.payload.data.Name;
                    }
                    if(action.payload.data.Gender !== ""){
                        state.genderLoad = action.payload.data.Gender;
                    }
                    if(action.payload.data.Phone !== ""){
                        state.phoneLoad = action.payload.data.Phone;
                    }
                    if(action.payload.data.Birthday !== ""){
                        state.birthdayLoad = action.payload.data.Birthday;
                    }
                break;
            case GET_ADDRESS:
                state.address = action.payload;
                break;
            case GET_PROVIDE:
                if(state.address){
                    state.provide = state.address.find(({name}) => name === action.payload);
                    state.district = state.provide.districts;
                    state.selectedProvide = action.payload;
                }
                break;
            case GET_DISTRICT:
                if(state.district){
                    const findWard = state.district.find(({name}) => name === action.payload);
                    state.ward = findWard.wards;
                    state.selectedDistrict = action.payload;
                }
                break;
            case GET_WARD:
                state.selectedWard = action.payload;
                break;
            case GET_ADDRESS_DETAIL:
                state.selectedAddress = action.payload;
                break;
            case SET_PAGE:
                state.page = action.payload;
                break;
            case SET_NAME:
                state.nameLoad = action.payload;
                break;
            case SET_BIRTHDAY:
                state.birthdayLoad = action.payload;
                break;
            case SET_GENDER:
                state.genderLoad = action.payload;
                break;
            case SET_PHONE:
                state.phoneLoad = action.payload;
                break;
            case PAYMENT_METHOD:
                state.payment = action.payload;
                break;
            case CREATE_ORDER_OF_CUSTOMER:
                state.newOrder = action.payload;
                break;
            case CONTINUE_SHOPPING_CLICK:
                state.newOrder = action.payload;
                break;
            case CHECK_ORDER_STATUS:
                state.orderStatus = action.payload;
                break;
            case GET_LENGTH:
                state.length = action.payload;
                break;
            default:
                break;
        }
        console.log(state)
    return {...state}
}
