import CreateAccount from "./Components/accountComponent/createAccount";
import CreateNewOrder from "./Components/adminPageComponent/InforComponents/createOrder";
import CreateProduct from "./Components/adminPageComponent/InforComponents/createProduct";
import DetailsOfCustomer from "./Components/adminPageComponent/InforComponents/customerInfor";
import OrderDetails from "./Components/adminPageComponent/InforComponents/orderDetails";
import ProductInfor from "./Components/adminPageComponent/InforComponents/productInfor";
import LoginAdmin from "./Components/adminPageComponent/loginAdminComponent/loginAdmin";
import CustomersInfo from "./Components/adminPageComponent/pageComponent/customer";
import OrdersInfo from "./Components/adminPageComponent/pageComponent/orders";
import ManagerProduct from "./Components/adminPageComponent/pageComponent/staff";
import Categories from "./Components/bodyComponent/bodyContent/Categories";
import CheckProductAndPayment from "./Components/checkProductComponent/checkAgainProduct";
import LoginForm from "./Components/modalComponent/loginForm";
import UserProfile from "./Components/userProfileComponent/userProfile";
import HomePageShop24h from "./pages/homePage";
import MoreProduct from "./pages/moreProducts";
import ProductDetails from "./pages/productDetail";

const routerList = [
    {path: "/", element: <HomePageShop24h/>},
    {path:"/Categories", element:<Categories/>},
    {path:"/Categories/More", element:<MoreProduct/>},
    {path:"/Categories/More/Details/:id", element:<ProductDetails/>},
    {path:"/Login", element:<LoginForm/>},
    {path:"/Cart", element:<CheckProductAndPayment/>},
    {path:"/User", element:<UserProfile/>},
    {path:"/Payment",  element:<UserProfile/>},
    {path:"/Account", element:<CreateAccount/>},
    
]
export const adminRouterList = [
    {path:"/Administrator", element:<OrdersInfo/>},
    {path:"/Administrator/Orders", element:<OrdersInfo/>},
    {path:"/Administrator/Customer/:userId", element:<DetailsOfCustomer/>},
    {path:"/Administrator/Customer", element:<CustomersInfo/>},
    {path:"/Administrator/Staff", element:<ManagerProduct/>},
    {path:"/Administrator/Staff/:productId", element:<ProductInfor/>},
    {path:"/Administrator/CreateProduct", element:<CreateProduct/>},
    {path:"/Administrator/CreateOrder", element: <CreateNewOrder/>},
    {path:"/Administrator/Orders/:orderId", element:<OrderDetails/>},
    {path:"/Administrator/Account", element:<CreateAccount/>},

]
export const loginAdminList = [
    {path:"/LoginAdmin", element:<LoginAdmin/>}
]
export default routerList;