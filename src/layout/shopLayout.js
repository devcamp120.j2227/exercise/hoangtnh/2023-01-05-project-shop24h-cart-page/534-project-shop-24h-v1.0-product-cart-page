import { ThemeProvider } from "@mui/material";
import { theme } from "../Components/userProfileComponent/userProfile";
import FooterShop24h from "../views/Footer";
import HeaderShop24h from "../views/Header";

const ShopLayout = ({children})=>{
    
    return (
        <ThemeProvider theme={theme}>
            <HeaderShop24h/>
                {children}
            <FooterShop24h/>
        </ThemeProvider>
    )
}
export default ShopLayout;