import {
    ALERT_STATUS, BUTTON_DELETE_HANDLER, CHANGE_STATUS_DELETE, CHANGE_STATUS_EDIT, CONFIRM_DELETE_ORDER_HANDLER,
    CREATE_NEW_ORDER,
    CUSTOMER_RESULT,
    DATE_FILTER, EDIT_PRODUCT_API, FILTER_BY_NAME_PRODUCT, FILTER_ORDER_BY_PHONE, FILTER_ORDER_BY_STATUS, FILTER_PRODUCT, GET_ALL_ORDERS, GET_ALL_PRODUCT, GET_CART_BAG, GET_CURRENT_PAGE, GET_CURRENT_PAGE_PRODUCT, GET_CUSTOMER_DETAIL, GET_DATE, GET_IMG_URL, GET_LENGTH, GET_LENGTH_PRODUCT, GET_NUMBER_PRODUCT_STATUS, GET_ORDER_INFO,
    GET_ORDER_RESULT,
    GET_PHONE_NUMBER_SEARCH, GET_PRODUCT_INFO, GET_QUANTITY_PRODUCT, GET_SHORT_ID, GET_STATUS_DELETE_IMG, GET_STATUS_ORDER, HANDLE_CHANGE_STATUS_ORDER, LOG_IN_ACCOUNT, LOG_OUT, LOG_OUT_ACCOUNT, OPEN_TOAST, REMOVE_PRODUCT_IN_CART, STATUS_ORDER,
    SUCCESS_LOGIN, WRONG_PASSWORD, WRONG_USERNAME
} from "../constants/account.const"

export default function getAccount(data, action) {
    return async (dispatch) => {

        if (data.username !== "" && data.password !== "") {
            var requestOptions = {
                method: 'POST',
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    username: data.username,
                    password: data.password
                }),
                redirect: 'follow'
            };
            try {
                const response = await fetch("http://localhost:8000/api/loginWithEncode", requestOptions)
                const data = await response.json();
                if (data.message === "username is incorrect") {
                    await dispatch({
                        type: WRONG_USERNAME,
                        payload: "username is incorrect"
                    })
                }
                if (data.message === "Password is incorrect") {
                    await dispatch({
                        type: WRONG_PASSWORD,
                        payload: "Password is incorrect"
                    })
                }
                if (data.message === "Login as admin") {
                    localStorage.setItem("refToken", data.data.refreshToken);
                    localStorage.setItem("admin", JSON.stringify(data));
                    await dispatch({
                        type: SUCCESS_LOGIN,
                        payload: data,
                    })
                    if (action) action()
                }
            } catch (error) {
                dispatch({
                    error: error
                })
            }
        }
    }
}
export const createNewAccount = (data, action) =>{
   return async (dispatch)=>{
    var requestOptions = {
        method: 'POST',
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({
            username: data.userName,
            password: data.password,
            email: data.email,
            role: "customer"
        }),
        redirect: 'follow'
      };
      
      try{
        const response = await fetch("http://localhost:8000/api/accounts", requestOptions);
        const data = await response.json()
        await dispatch({
            type: OPEN_TOAST,
            payload: {
                open: true,
                message: "Create new account successfull",
                type: "success"
            }
        })
        localStorage.setItem("userAccount", JSON.stringify(data));
        if(action) action()
      }catch(error){
        console.log(error)
      }
   }
}
export const loginAccount =(user, action)=>{
    return async (dispatch)=>{
        if (user.username === "") {
          return  dispatch(openToast({
                open: true,
                message: "User name is required",
                type: "error"
            }))
        }
        if (user.password === "") {
          return  dispatch(openToast({
                open: true,
                message: "Password is required",
                type: "error"
            }))
        }
      try{
        const requestOptions = {
            method: 'POST',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                username: user.username,
                password:user.password
            }),
            redirect: 'follow'
          };
        const response = await fetch("http://localhost:8000/api/loginWithEncode", requestOptions);
        const data = await response.json();
        console.log(data)
        if(data.message ==="Password is incorrect"){
          return  dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "password is incorrect",
                    type: "error"
                }
            })
        }
        if(data.message ==="username is incorrect"){
            return  dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "username is incorrect",
                    type: "error"
                }
              })
          }
        if(data.message ==="true password, Login successful"){
            await dispatch({
                type: LOG_IN_ACCOUNT,
                payload: data
            })
            await dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Login successfull",
                    type: "success"
                }
            })
            localStorage.setItem("userAccount", JSON.stringify(data));
            if(action) {action()}
        }
      }catch(error){
        console.log(error)
      }
    }
}
export const openStatusAlert = (data) => {
    return {
        type: ALERT_STATUS,
        payload: data
    }
}
export const logOutCustomer = ()=>{
    return{
        type: LOG_OUT_ACCOUNT,
        payload: null
    }
}
export const logoutAccount = (refToken) => {
    return async (dispatch) => {
        var requestOptions = {
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                refToken: refToken
            }),
            redirect: 'follow'
        };
        try {
            const response = await fetch("http://localhost:8000/api/logout", requestOptions)
            const data = await response.json();

            if (data.message !== 'RefToken is incorrect') {
                console.log(data)
            }
            if (data.message === 'RefToken is incorrect') {
                console.log(data)
                await dispatch({
                    type: LOG_OUT,
                    payload: ""
                })
            }
            if (data.message === 'Log out successfully') {
                localStorage.removeItem("refToken");
                localStorage.removeItem("admin");
                await dispatch({
                    type: LOG_OUT,
                    payload: ""
                })
            }
        } catch (error) {
            dispatch({
                error: error
            })
        }
    }
}
export const getAllOrders = (data) => {
    return {
        type: GET_ALL_ORDERS,
        payload: data
    }
}
export const getLength = (data) => {
    return {
        type: GET_LENGTH,
        payload: data
    }
}
export const getShortOrderId = (orderId, currentPage) => {
    return async (dispatch) => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        if (orderId.length > 1) {
            try {
                const response = await fetch("http://localhost:8000/api/orders/filter/" + orderId, requestOptions)
                const data = await response.json();
                await dispatch({
                    type: GET_SHORT_ID,
                    payload: data.data
                })
            } catch (error) {
                dispatch({
                    error: error
                })
            }
        }
        if (orderId === "" || orderId.length === 1) {
            dispatch({
                type: STATUS_ORDER,
                payload: "All Status"
            })
            try {
                const response = await fetch("http://localhost:8000/api/orders", requestOptions)
                const data = await response.json();
                await dispatch({
                    type: GET_LENGTH,
                    payload: data.data
                })
            } catch (error) {
                dispatch({
                    error: error
                })
            }
            try {
                const response = await fetch("http://localhost:8000/api/orders?skipNumber=" + ((currentPage - 1) * 5) + "&limitNumber=" + 5)
                const data = await response.json();
                await dispatch({
                    type: GET_ALL_ORDERS,
                    payload: data.data
                })
            } catch (error) {
                dispatch({
                    error: error
                })
            }
        }
    }
}
export const getCurrentPage = (data) => {
    return {
        type: GET_CURRENT_PAGE,
        payload: data
    }
}
export const filterOrderByStatus = (status) => {
    return async (dispatch) => {
        dispatch({
            type: STATUS_ORDER,
            payload: status
        })
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        if (status !== "All Status") {
            try {
                const response = await fetch("http://localhost:8000/api/filter?statusOrder=" + status, requestOptions)
                const data = await response.json();
                await dispatch({
                    type: FILTER_ORDER_BY_STATUS,
                    payload: data.data
                })
            } catch (error) {
                await dispatch({
                    error: error
                })
            }
            if (status === "All Status") {
                dispatch({
                    type: STATUS_ORDER,
                    payload: "All Status"
                })
            }
        }
    }
}
export const setStatusEdit = (status) => {
    return {
        type: CHANGE_STATUS_EDIT,
        payload: status
    }
}
export const getOrderInfo = (order) => {
    return {
        type: GET_ORDER_INFO,
        payload: order
    }
}
export const handleChangeStatusOrder = (statusOrder) => {
    return {
        type: GET_STATUS_ORDER,
        payload: statusOrder
    }
}
export const fetchApiChangeStatusOrder = (statusOrder, orderId) => {
    return async (dispatch) => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: JSON.stringify({ "status": `${statusOrder}` }),
            redirect: 'follow'
        };
        try {
            const response = await fetch("http://localhost:8000/api/orders/edit?orderId=" + orderId, requestOptions);
            const data = await response.json();
            await dispatch({
                type: HANDLE_CHANGE_STATUS_ORDER,
                payload: ""
            })
        }
        catch (error) {
            await dispatch({
                error: error
            })
        }
    }
}
export const openToast = (data) => {
    return {
        type: OPEN_TOAST,
        payload: data
        }
}

//actiom khi nút delete được bấm
export const deleteOrderHandler = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: BUTTON_DELETE_HANDLER,
            data: data,
        })
    }
}
//thay đổi trạng thái đóng mở delete modal
export const setStatusDelete = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: CHANGE_STATUS_DELETE,
            data: data
        })
    }
}
//xác nhận xóa order
export const confirmDeleteOrderHandler = (orderId) => {
    return async (dispatch) => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        const response = await fetch("http://localhost:8000/api/ordersCustomer/" + orderId, requestOptions);
        const data = await response.json();
        const userId = data.data._id;
        var body = {
            method: 'DELETE',
            redirect: 'follow'
        };
        try {
            const response = await fetch("http://localhost:8000/api/orders/delete?orderId=" + orderId + "&userId=" + userId, body);
            dispatch({
                type: CONFIRM_DELETE_ORDER_HANDLER,
                response: response.message
            })
        } catch (error) {
            return dispatch({
                error: error
            })
        }

    }
}
export const filterOrderByPhoneNumber = (phoneNumber) => {
    return async (dispatch) => {
        dispatch({
            type: GET_PHONE_NUMBER_SEARCH,
            payload: phoneNumber
        })
        var requestOptions = {
            method: 'GET',
            headers: { "Content-Type": "application/json" },
            redirect: 'follow'
        };
        try {
            fetch("http://localhost:8000/api/orders/phoneNumber/" + phoneNumber, requestOptions)
                .then(data => data.json())
                .then(async data => {
                    if (data) {
                        console.log(data)
                        const orderId = data.data.Carts
                        const promises = orderId.map(async value => {
                            const result = await fetch("http://localhost:8000/api/orders/" + value)
                            return result.json()
                        });
                        //map, filter, find,... sẽ trả ra promises 
                        //method Promise.all sẽ chạy đồng thời các promises ở result - tác dụng chạy tất cả cùng 1 lúc tiết kiệm thời gian
                        const orders = await Promise.all(promises);
                        var orderData = orders.map(value => {
                            return value.data
                        })
                        return dispatch({
                            type: FILTER_ORDER_BY_PHONE,
                            payload: orderData
                        })
                    }
                })
        } catch (error) {
            dispatch({
                error: error
            })
        }
    }
}
export const getDateResult = (date, currentPage, limit) => {
    //hàm chuyển lại ngày tháng năm
    function convert(str) {
        if(str === null){
            return ""
        }
        else{
            var date = new Date(str);
            var month = ("0" + (date.getMonth() + 1)).slice(-2);
            var day = ("0" + date.getDate()).slice(-2);
        }
        return [date.getFullYear(), month, day].join("-");
    }
    return async (dispatch) => {
        const startDate = (convert(date[0].startDate));
        const endDate = (convert(date[0].endDate));
        dispatch({
            type: GET_DATE,
            payload: date
        })
        var requestOptions = {
            method: 'GET',
            headers: { "Content-Type": "application/json" },
            redirect: 'follow'
        };
        try {
            
            const response = await fetch("http://localhost:8000/api/filterDate/dateOrder?startDate="+ startDate +"&endDate=" + endDate +"&skipNumber=" +((currentPage-1) * limit)+"&limitNumber=" + limit, requestOptions)
            const orderData = await response.json();
            await dispatch({
                type: DATE_FILTER,
                payload: orderData.data
            })
        } catch (error) {
           console.log(error)
        }
        try{
            const response = await fetch("http://localhost:8000/api/filterDate/dateOrder?startDate="+ startDate +"&endDate=" + endDate, requestOptions);
            const data = await response.json();
            console.log(data)
            await dispatch({
                type: GET_LENGTH,
                payload: data.data
            })
        }catch(error){
            console.log(error)
        }
    }
}
//get data all customers
export const getAllCustomers = () => {
    return async (dispatch) => {
        var requestOptions = {
            method: 'GET',
            headers: { "Content-Type": "application/json" },
            redirect: 'follow'
        };
        try {
            const response = await fetch("http://localhost:8000/api/allCustomers", requestOptions);
            const data = await response.json()
            return dispatch({
                type: CUSTOMER_RESULT,
                payload: data.data
            })
        } catch (error) {
            return dispatch({
                error: error
            })
        }
    }
}
//get data of customer by id
export const getCustomerById = (userId) => {
    return async (dispatch) => {
        var requestOptions = {
            method: 'GET',
            headers: { "Content-Type": "application/json" },
            redirect: 'follow'
        };
        try {
            const response = await fetch("http://localhost:8000/api/customer&orders?userId=" + userId, requestOptions);
            const data = await response.json()
            return dispatch({
                type: GET_CUSTOMER_DETAIL,
                payload: data.data
            })
        } catch (error) {
            return dispatch({
                error: error
            })
        }
    }
}
//get all products 
export const getAllProduct = (currentPage, limit) => {
    return async (dispatch) => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        try {
            //lấy all product để lấy length phân trang
            const response = await fetch("http://localhost:8000/api/skip-limit-products", requestOptions);
            const data = await response.json();
             dispatch({
                type: GET_LENGTH_PRODUCT,
                payload: data.data.length
            })
            //get product theo skip limit phân trang
            try{
                const response = await fetch("http://localhost:8000/api/skip-limit-products?skipNumber="+((currentPage-1) * limit)+"&limitNumber=" + limit);
                const data = await response.json();
                dispatch({
                    type: GET_ALL_PRODUCT,
                    payload: data.data
                })
            }
            catch(error) {
                dispatch({
                    error: error
                })
            }
        } catch (error) {
            dispatch({
                error: error
            })
        }
    }
}
//thay đổi number chuông thông báo 
export const getNumberProductNoti = (data) => {
    return {
        type: GET_NUMBER_PRODUCT_STATUS,
        payload: data
    }
}
export const getCurrentPageProduct = (data) => {
    return {
        type: GET_CURRENT_PAGE_PRODUCT,
        payload: data
    }
}
export const filterProduct =(description, currentPage, limit)=>{
    return async(dispatch)=>{
        dispatch({
            type: FILTER_PRODUCT,
            payload: description
        })
        if(description !=="All"){
            try{
                const response = await fetch("http://localhost:8000/api/products/filter?skipNumber="+((currentPage-1) * limit)+"&limitNumber=" + limit +"&description="+ description);
                const data = await response.json();
                console.log(data)
                dispatch({
                    type: GET_ALL_PRODUCT,
                    payload: data.data
                })
                try{
                    const response = await fetch("http://localhost:8000/api/products/filter?"+"description="+ description);
                    const data = await response.json();
                    dispatch({
                        type: GET_LENGTH_PRODUCT,
                        payload: data.data.length
                    })
                }
                catch(error) {
                    dispatch({
                        error: error
                    })
                }
            }
            catch(error) {
                dispatch({
                    error: error
                })
            }
        }
        if(description ==="All"){
            dispatch(getAllProduct(currentPage, limit))
        }
    }
}
export const getProductInfo = (productId) =>{
    return async(dispatch)=>{
        var requestOptions = {
            method: 'GET',
            headers: { "Content-Type": "application/json" },
            redirect: 'follow'
        };
        try {
            const response = await fetch("http://localhost:8000/api/products/" + productId, requestOptions);
            const data = await response.json()
            return dispatch({
                type: GET_PRODUCT_INFO,
                payload: data.data
            })
        } catch (error) {
            return dispatch({
                error: error
            })
        }
    }
}
//call api update product
export const callApiEditProduct = (productInfo, productId)=>{
    console.log(productInfo, productId)
    return async(dispatch)=>{
        const adminInfor = JSON.parse(localStorage.getItem("admin"));
        console.log(adminInfor)
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${adminInfor.data.accessToken}`);
        myHeaders.append("Content-Type", "application/json");
        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: JSON.stringify({
                Name: productInfo.Name,
                Type: productInfo.Type,
                ImageUrl: productInfo.ImageUrl,
                BuyPrice: productInfo.BuyPrice,
                PromotionPrice: productInfo.PromotionPrice,
                Description: productInfo.Description,
                ProductDescription: productInfo.ProductDescription,
                Warranty: productInfo.Warranty,
                DeliveryFee: productInfo.DeliveryFee,
                DeliverTime: productInfo.DeliverTime
            }),
            redirect: 'follow'
          };
        try{
            const response = await fetch("http://localhost:8000/api/products/"+ productId, requestOptions);
            const data = await response.json();
            if(data.message=== 'Token is not valid'){
                var getNewAccessToken = {
                    method: 'POST',
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({
                        token: adminInfor.data.refreshToken,
                    }),
                    redirect: 'follow'
                };
                const response = await fetch("http://localhost:8000/api/refreshTokens", getNewAccessToken);
                const newAccessToken = await response.json();
                console.log(newAccessToken);
                //newAccessToken.data.accessToken
                adminInfor.data.accessToken = newAccessToken.data.accessToken;
                localStorage.setItem("admin",JSON.stringify(adminInfor));
                const myHeaders = new Headers();
                myHeaders.append("Authorization", `Bearer ${newAccessToken.data.accessToken}`);
                myHeaders.append("Content-Type", "application/json");
                const requestOptions = {
                    method: 'PUT',
                    headers: myHeaders,
                    body: JSON.stringify({
                        Name: productInfo.Name,
                        Type: productInfo.Type,
                        ImageUrl: productInfo.ImageUrl,
                        BuyPrice: productInfo.BuyPrice,
                        PromotionPrice: productInfo.PromotionPrice,
                        Description: productInfo.Description,
                        ProductDescription: productInfo.ProductDescription,
                        Warranty: productInfo.Warranty,
                        DeliveryFee: productInfo.DeliveryFee,
                        DeliverTime: productInfo.DeliverTime
                    }),
                    redirect: 'follow'
                  };
                  try{
                    const response = await fetch("http://localhost:8000/api/products/"+ productId, requestOptions);
                    const data = await response.json();
                    console.log("new access token",data);
                      if (data.status === "you are not premission") {
                          return dispatch({
                              type: OPEN_TOAST,
                              payload: {
                                  open: true,
                                  message: "you are not premission",
                                  type: "warning"
                              }
                          })
                    }
                    dispatch({
                        type: EDIT_PRODUCT_API,
                        payload: data
                    })
                    }
                    catch(error){
                        console.log(error)
                    }
            }
            if (data.status === "you are not premission") {
                return dispatch({
                    type: OPEN_TOAST,
                    payload: {
                        open: true,
                        message: "you are not premission",
                        type: "warning"
                    }
                })
            }
            else {
                console.log(data)
                dispatch({
                    type: EDIT_PRODUCT_API,
                    payload: data
                })
            }
        }catch(error){
            dispatch({
                error: error
            })
        }
    }
}
export const getStatusDeleteImg = (data) =>{
    return {
        type: GET_STATUS_DELETE_IMG,
        payload: data
    }
}
export const getImgUrlHandler = (url) =>{
    return {
        type: GET_IMG_URL,
        payload: url
    }
}
export const createNewProduct = (data)=>{
    return async(dispatch) =>{
        const newProduct = data;
        if(newProduct.Name ===""){
          return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Product Name is required",
                    type: "warning"
                }
            })
        }
        if(newProduct.Type ===""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Product Type is required",
                    type: "warning"
                }
            })
        }
        if(newProduct.Description ===""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Description is required",
                    type: "warning"
                }
            })
        }
        if(newProduct.BuyPrice === 0){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Buy Price is required",
                    type: "warning"
                }
            })
        }
        if(newProduct.PromotionPrice === 0){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Promotion Price is required",
                    type: "warning"
                }
            })
        }
        if(newProduct.PromotionPrice >= newProduct.BuyPrice){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Promotion Price can't be more than Buy Price",
                    type: "warning"
                }
            })
        }
        if(newProduct.ImageUrl ===null){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Image Product is required",
                    type: "warning"
                }
            })
        }
        if(newProduct.DeliveryFee ===""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Delivery Fee is required",
                    type: "warning"
                }
            })
        }
        if(newProduct.DeliverTime ===""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Deliver Time is required",
                    type: "warning"
                }
            })
        }
        if(newProduct.ProductDescription ===""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Product Description is required",
                    type: "warning"
                }
            })
        }
        if(newProduct.Warranty ===""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Warranty is required",
                    type: "warning"
                }
            })
        }
        else{
            var requestOptions = {
                method: 'POST',
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({
                    Name: newProduct.Name,
                    Type: newProduct.Type,
                    ImageUrl: newProduct.ImageUrl,
                    BuyPrice: newProduct.BuyPrice,
                    PromotionPrice: newProduct.PromotionPrice,
                    Description: newProduct.Description,
                    ProductDescription: newProduct.ProductDescription,
                    Warranty: newProduct.Warranty,
                    DeliveryFee: newProduct.DeliveryFee,
                    DeliverTime: newProduct.DeliverTime
                }),
                redirect: 'follow'
              };
            try{
                const response = await fetch("http://localhost:8000/api/products", requestOptions);
                //const response = await fetch("https://data.mongodb-api.com/app/data-wufyb/endpoint/data/v1/api/products", requestOptions);
                const data = await response.json();
                console.log(data)
                if(data.status ==='Internal server error'){
                    dispatch({
                        type: OPEN_TOAST,
                        payload: {
                            open: true,
                            message: "Product name is duplicate",
                            type: "error"
                        }
                    })
                }
                else{
                    dispatch({
                        type: OPEN_TOAST,
                        payload: {
                            open: true,
                            message: "Create New Order Successfully",
                            type: "success"
                        }
                    })
                }
            }catch(error){
                dispatch({
                    error: error
                })
            }
        }
    }
}
export const filterByProductName= (productName)=>{
    return async (dispatch)=>{
        dispatch({
            type: FILTER_BY_NAME_PRODUCT,
            payload: productName
        })
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };
          try{
            const response = await fetch("http://localhost:8000/api/products/filter?nameProduct="+ productName, requestOptions)
            const data = await response.json()
            dispatch({
                type: GET_ALL_PRODUCT,
                payload: data.data
            });
            dispatch({
                type:GET_LENGTH_PRODUCT,
                payload: data.data
            })
          }
          catch(error){
            return dispatch({
                error: error
            })
          }
          if(productName === ""){
            dispatch(getAllProduct(1, 5))
        }
    }
}
export const getQuantity = (data) =>{
    return {
        type: GET_QUANTITY_PRODUCT,
        payload: data
    }
}
export const getCartBag = (data)=>{
    return {
        type: GET_CART_BAG,
        payload: data
    }
}
export const onBtnRemoveProduct =(id)=>{
    return {
        type: REMOVE_PRODUCT_IN_CART,
        payload: id
    }
}
export const createNewOrder = (data, cartBag) =>{
    return async(dispatch)=>{
        const customer = data;
        console.log(cartBag)
        //validate customer
        if(customer.Name === ""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Customer name is required",
                    type: "warning"
                }
            })
        }
        if(customer.Email ===""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Email is required",
                    type: "warning"
                }
            })
        }
        if(customer.Phone === ""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Phone number is required",
                    type: "warning"
                }
            })
        }
        if(customer.Address.Provide === "" ){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Provide is required",
                    type: "warning"
                }
            })
        }
        if(customer.Address.District === ""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "District is required",
                    type: "warning"
                }
            })
        }
        if(customer.Address.Ward === ""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Ward is required",
                    type: "warning"
                }
            })
        }
        if(customer.Address.Address === ""){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Address is required",
                    type: "warning"
                }
            })
        }
        if(cartBag.length === 0){
            return dispatch({
                type: OPEN_TOAST,
                payload: {
                    open: true,
                    message: "Cart Bag can't be emty",
                    type: "warning"
                }
            })
        }
        //all condition validated
        else{
            var newCustomer = {
                method: 'POST',
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(customer),
                redirect: 'follow'
            }; 
            try{
                const response = await fetch("http://localhost:8000/api/customers-admin-page", newCustomer);
                const data = await response.json();
                if(data.status === 'Internal server error'){ //nếu customer đã mua hàng trước có lưu email
                    const requestOptions = {
                        method: 'GET',
                        redirect: 'follow'
                    };
                    //call api lấy customer id bằng email
                    try{
                        const response = await fetch("http://localhost:8000/api/email-customer?email="+ customer.Email, requestOptions);
                        const customerFound = await response.json();
                        const customerId = customerFound.data.UserId;
                        //sau khi lấy thành công customer id create new order vào customer đấy
                        const requestPostOptions = {
                            method: 'POST',
                            headers: {
                                'Content-type': 'application/json; charset=UTF-8'
                            },
                            body: JSON.stringify({
                                Orders: cartBag
                                }),
                            redirect: 'follow'
                        };
                        try {
                            const responsePost =  await fetch("http://localhost:8000/api/orders/post?userId="+ customerId, requestPostOptions);
                            const data = await responsePost.json();
                                dispatch({
                                type: CREATE_NEW_ORDER,
                                payload: data.data,
                            })
                        } catch (error) {
                                dispatch({
                                error: error
                            })
                        }
                    }catch(error){
                        console.log("Error found customer by email")
                    }
                }
                else{//nếu customer là khách hàng mới
                    const customerId = data.data.UserId;
                    const requestPostOptions = {
                        method: 'POST',
                        headers: {
                            'Content-type': 'application/json; charset=UTF-8'
                        },
                        body: JSON.stringify({
                            Orders: cartBag
                            }),
                        redirect: 'follow'
                    };
                    try {
                        const responsePost =  await fetch("http://localhost:8000/api/orders/post?userId="+ customerId, requestPostOptions);
                        const data = await responsePost.json();
                            dispatch({
                            type: CREATE_NEW_ORDER,
                            payload: data.data,
                        })
                    } catch (error) {
                            dispatch({
                            error: error
                        })
                    }
                }
            }
            catch(error){
                console.log(error)
            }
        }
    }
}
//get customer by order Id
export const getCustomerByOrderId = (id)=>{
    return async(dispatch)=>{
        const requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        //call api lấy customer id bằng orderId
        try{
            const response = await fetch("http://localhost:8000/api/ordersCustomer/"+ id, requestOptions);
            const customerFound = await response.json();
            console.log(customerFound)
            dispatch({
                type: GET_CUSTOMER_DETAIL,
                payload: customerFound.data
            })
            
        }
        catch(error){
            console.log(error)
        }  
    }
}
export const getOrderResult =(id) =>{
    return async(dispatch)=>{
        const requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        try{
            const responseOrder = await fetch("http://localhost:8000/api/orders/"+ id, requestOptions);
            const data = await responseOrder.json();
            dispatch({
                type: GET_ORDER_RESULT,
                payload: data.data
            })
        }catch(error){
            console.log(error)
        }  
    }
}
