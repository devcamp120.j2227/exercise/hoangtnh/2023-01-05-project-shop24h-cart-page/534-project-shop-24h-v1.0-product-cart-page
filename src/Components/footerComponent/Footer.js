import { Col, Container, Row } from "reactstrap"
import About from "./FooterContent/About";
import HomeDecor from "./FooterContent/HomeDecor";
import Information from "./FooterContent/Information";
import NewsLetter from "./FooterContent/NewsLetter";

const Footer = () => {
    return (
        <div style={{backgroundColor:"#d1c286"}}>
            <Container >
                <div className="text-center" style={{paddingTop:"50px"}} >
                    <h3 style={{color:"white"}}>Devcamp COSY-HOME</h3>
                    <div className="d-flex justify-content-center">
                        <hr style={{width:"600px"}}/>
                    </div>
                </div>
                <Row md={12} sm={12} lg={12} xs={12}>
                    <Col sm={3}>
                        <About/>
                    </Col>
                    <Col sm={3}>
                        <Information/>
                    </Col>
                    <Col sm={3}>
                        <HomeDecor/>
                    </Col>
                    <Col sm={3}>
                        <NewsLetter/>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default Footer;