import { Button, Col, Row, Breadcrumb, BreadcrumbItem } from "reactstrap";
import { useState } from "react";
import imgCategory from "../../../assets/slideImg/slide3.webp"
import '@fortawesome/fontawesome-free/css/all.min.css';
import data from "../../../assets/data/ProductData.json";
import { Link, useNavigate } from "react-router-dom";
import { Container } from "@mui/material";

// const fetchApi = async(url, body) =>{
//     const response = await fetch(url,body);
//     const data = await response.json();
    
//     return data;
// }

const Categories = () =>{
    const [Product, showProduct] = useState([]);
    const [btnClick, setStatus] = useState(false);
    const [btnMore, setStatusDefault] = useState(false);
    const navigate = useNavigate();
    const livingRoomBtn = () =>{
        //Code sử dụng database từ mongoose được tạo từ node.js bài 520.10 
    // fetchApi("http://localhost:8000/api/products/description?DescriptionProduct=LivingRoom")
    //     .then((data) =>{
    //         showProduct(data.Product)
    //         setStatus(true);
    //     })
    //     .catch((error)=>{
    //         console.log(error.message)
    //     })
    
    //code sử dụng data từ data json được tạo
    let result = data.Products.filter( ({Description}) => Description ==="LivingRoom");
    showProduct(result);
    setStatus(true);
    }
    const bedRoomBtn = ()=>{
        //Code sử dụng database từ mongoose được tạo từ node.js bài 520.10 
    // fetchApi("http://localhost:8000/api/products/description?DescriptionProduct=BedRoom")
    // .then((data) =>{
    //     showProduct(data.Product)
    //     setStatus(true);
    // })
    // .catch((error)=>{
    //     console.log(error.message)
    // })

        //code sử dụng data từ data json được tạo
    let result = data.Products.filter( ({Description}) => Description ==="BedRoom");
    showProduct(result);
    setStatus(true);
    }
    const LoftRoomBtn = ()=>{
        //Code sử dụng database từ mongoose được tạo từ node.js bài 520.10 
    // fetchApi("http://localhost:8000/api/products/description?DescriptionProduct=LoftRoom")
    // .then((data) =>{
    //     showProduct(data.Product)
    //     setStatus(true);
    // })
    // .catch((error)=>{
    //     console.log(error.message)
    // })
            //code sử dụng data từ data json được tạo
        let result = data.Products.filter( ({Description}) => Description ==="LoftRoom");
        showProduct(result);
        setStatus(true);
    }
    
    const KitchenBtn = ()=>{
        //Code sử dụng database từ mongoose được tạo từ node.js bài 520.10 
        // fetchApi("http://localhost:8000/api/products/description?DescriptionProduct=Kitchen")
        // .then((data) =>{
        //     showProduct(data.Product)
        //     setStatus(true);
        // })
        // .catch((error)=>{
        //     console.log(error.message)
        // })
             //code sử dụng data từ data json được tạo
        let result = data.Products.filter( ({Description}) => Description ==="Kitchen");
        showProduct(result);
        setStatus(true);
    }
    return (
        <div className="div-categories">
            <Container md={12} sm={12} lg={12} xs={12}>
                <Breadcrumb>
                    <BreadcrumbItem > 
                        <Link to="/" style={{textDecoration:"none"}}>Home</Link>
                    </BreadcrumbItem>
                    <BreadcrumbItem>
                        <Link to="/Categories">Categories</Link>
                    </BreadcrumbItem>
                </Breadcrumb>
                <Row md={12} sm={12} lg={12} xs={12}>
                    <Col style={{marginTop:"50px"}} md={5} sm={5} lg={5} xs={5}>
                        <Row className="justify-content-center">
                            <Col xs={3} style={{marginRight:"40px"}}>
                                <h4>Categories</h4>
                            </Col>
                            <Col xs={3}>
                                <hr style={{width:"50px",border:"1px solid orange"}}/>
                            </Col>
                        </Row>
                        <Row style={{marginTop:"50px"}}>
                            <Row className="d-flex align-items-center mt-5" >
                                <Col xs={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button" onClick={livingRoomBtn} style={{margin:"0px",color: "#d1c286"}}>01</p>
                                </Col>
                                <Col xs={1}>
                                    <hr style={{width:"30px",border:"1px solid orange"}}/>
                                </Col>
                                <Col xs={3}>
                                    <p type="button" onClick={livingRoomBtn} style={{margin:" 0 0 0 10px"}}><b>Living Room</b></p>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Row className="d-flex align-items-center mt-5">
                                <Col xs={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button" onClick={bedRoomBtn} style={{margin:"0px",color: "#d1c286"}}>02</p>
                                </Col>
                                <Col xs={1}>
                                    <hr style={{width:"30px",border:"1px solid orange"}}/>
                                </Col>
                                <Col xs={3}>
                                    <p type="button" onClick={bedRoomBtn} style={{margin:" 0 0 0 10px"}}><b>Bed Room</b></p>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Row className="d-flex align-items-center mt-5">
                                <Col xs={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button" onClick={LoftRoomBtn} style={{margin:"0px",color: "#d1c286"}}>03</p>
                                </Col>
                                <Col xs={1}>
                                    <hr style={{width:"30px",border:"1px solid orange"}}/>
                                </Col>
                                <Col xs={3}>
                                    <p type="button" onClick={LoftRoomBtn} style={{margin:" 0 0 0 10px"}}><b>Loft Room</b></p>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Row className="d-flex align-items-center mt-5">
                                <Col xs={1} style={{display:"flex",padding:"0px", justifyContent:"flex-end"}}>
                                    <p type="button" onClick={KitchenBtn} style={{margin:"0px",color: "#d1c286"}}>04</p>
                                </Col>
                                <Col xs={1}>
                                    <hr style={{width:"30px",border:"1px solid orange"}}/>
                                </Col>
                                <Col xs={3}>
                                    <p type="button" onClick={KitchenBtn} style={{margin:" 0 0 0 10px"}}><b>Kitchen</b></p>
                                </Col>
                            </Row>
                        </Row>
                        <Row>
                            <Button style={{width:"60%",color:"#d1c286",marginTop:"100px"}} onClick={()=>{setStatusDefault(true); setStatus(false); navigate("/Categories/More")}}> For More Product <i className="fa-solid fa-angles-right"></i></Button>
                        </Row>
                    </Col>
                    <Col style={{marginTop:"100px"}} md={7} sm={7} lg={7} xs={7}>
                        <Row style={{display: "flex" ,flexDirection: "row"}}>
                        {btnClick === true ? Product.map((value,index)=>{
                            return(
                                <Col sm={4} key={index}  className="d-flex justify-content-center" >
                                    <div className="text-center img-product">
                                        <img src={value.ImageUrl} style={{width:"160px", height:"160px"}}/>
                                        <div style={{marginTop:"10px"}}>
                                            <h6 style={{margin:"0px"}}>{value.Name}</h6>
                                            <p style={{textDecoration: "line-through", fontSize:"smaller", margin:"0px"}}>${value.BuyPrice}</p>
                                            <p style={{marginTop:"-5px"}}>${value.PromotionPrice}</p>
                                        </div>
                                    </div>
                                </Col>
                            )
                        }):<img src={imgCategory} style={{borderRadius:"5px", padding:"0px"}}/>}
                        </Row>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
export default Categories;