import { Grid, TextField, Select, MenuItem, InputLabel, FormControl, Button, ThemeProvider,Modal, Box, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAddressDetail, getDistrict, getProvide, getWard, setPage } from "../../actions/product.action";
import { theme } from "./userProfile";
import {style} from "./userPayment";
import avatarDefault from "../../assets/default avatar/png-transparent-default-avatar-thumbnail.png"

const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    
    return data;
}

const Address = () =>{
    const dispatch = useDispatch();
    const {ward, address, district, selectedDistrict, selectedProvide, selectedWard, selectedAddress, user} = useSelector((reduxData)=>
        reduxData.productReducer
    );
    const {accountUser} =  useSelector((reduxData)=>
        reduxData.accountReducer
    );
    const [open, setOpen] = useState(false);
    const handleClose = () => setOpen(false);
    const [uid, setUid] = useState("");
    const [email, setEmail] = useState("");
    
    useEffect(()=>{
        if(accountUser !== null){
            setUid(accountUser.exitUser._id)
            setEmail(accountUser.exitUser.Email);
        }
        if(user !== null){
            setUid(user.uid)
            setEmail(user.email)
        }
    }, [accountUser, user])
    const onBtnSaveHandler = () =>{
        var requestOptions = {
            method: 'PUT',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                Address: {Provide: `${selectedProvide}`, District: `${selectedDistrict}`,Ward: `${selectedWard}`,Address:  `${selectedAddress}`},
            }),
            redirect: 'follow'
          };
          if(selectedProvide !== "" && selectedDistrict !=="" && selectedWard !== "" && selectedAddress !== ""){
            fetchApi("http://localhost:8000/api/customers?userId="+ uid, requestOptions)
                .then((data)=>{
                    dispatch(getProvide(data.data.Address.Provide)); 
                    dispatch(getDistrict(data.data.Address.District)); 
                    dispatch(getWard(data.data.Address.Ward)); 
                    dispatch(getAddressDetail(data.data.Address.Address));
                    dispatch(setPage("payment"))
                    if(data.data === null){
                        const requestOptions = {
                            method: 'POST',
                            headers: {
                                'Content-type': 'application/json; charset=UTF-8'
                            },
                            body: JSON.stringify({
                                UserId: uid,
                                Email: email,
                                Address: {Provide: `${selectedProvide}`, District: `${selectedDistrict}`,Ward: `${selectedWard}`,Address:  `${selectedAddress}`},
                                }),
                            redirect: 'follow'
                            };
                        fetchApi("http://localhost:8000/api/customers", requestOptions)
                                .then((data)=>{
                                    dispatch(setPage("payment"))
                                })
                                .catch((error)=>{
                                    console.log(error.message);
                                })
                    };

                })
            .catch((error)=>{
                console.log(error.message);
            })
        }
        else{
            setOpen(true);
        }
          
    }
    const handleProvideChange = (e) =>{
        dispatch(getProvide(e.target.value));
    }
    const handleDistrictChange = (e) =>{
        dispatch(getDistrict(e.target.value));
    }
    const handleWardChange = (e)=>{
        dispatch(getWard(e.target.value))
    }
    const onAddressHandler = (e) =>{
        dispatch(getAddressDetail(e.target.value))
    }
    useEffect(()=>{
        fetchApi("http://localhost:8000/api/customers?userId="+ uid)
            .then((data)=>{
                console.log(data)
                dispatch(getProvide(data.data.Address.Provide));
                dispatch(getDistrict(data.data.Address.District));
                dispatch(getWard(data.data.Address.Ward));
                dispatch(getAddressDetail(data.data.Address.Address));
            })
            .catch((error)=>{
                console.log(error.message)
            });
    },[user, accountUser, uid])
    return(
            <ThemeProvider theme ={theme}>
                <Grid container direction="column"
                        justifyContent="flex-start" alignItems="flex-start"
                        md={9} sm={9} lg={9} xs={9} style={{backgroundColor:"whiteSmoke", padding: 10}}>
                    <Grid item> 
                        <h3><b>My Address</b></h3>
                    </Grid>
                    <Grid item style={{fontSize: 12, fontStyle:"italic"}}>
                        Choose your address to receive goods
                    </Grid>
                    <br/>
                    <Grid container spacing={2} direction="column"  md={12} sm={12} lg={12} xs={12}>
                        <Grid item>
                            <Grid container direction="row" alignItems="center" spacing={1}>
                                    <Grid item xs={2}>
                                        Province: 
                                    </Grid>
                                    <Grid item>
                                        <FormControl fullWidth>
                                            <InputLabel>Choose province</InputLabel>
                                            <Select
                                                label="Choose province"
                                                value={selectedProvide}
                                                onChange={handleProvideChange}
                                                sx={{width:250}}
                                                >
                                                {address.map((value, index)=>{
                                                    return(
                                                        <MenuItem value={value.name} key={index}>{value.name}</MenuItem>
                                                    )
                                                })}
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                </Grid>
                            </Grid>
                        <Grid item >
                            <Grid container direction="row" alignItems="center" spacing={1}>
                                <Grid item xs={2}>
                                    District: 
                                </Grid>
                                <Grid item>
                                    <FormControl fullWidth>
                                        <InputLabel>Choose district</InputLabel>
                                        <Select
                                            value={selectedDistrict}
                                            label="Choose province"
                                            onChange={handleDistrictChange}
                                            sx={{width:250}}
                                        >
                                            {district.map((value, index)=>{
                                                return(
                                                    <MenuItem value={value.name} key={index}>{value.name}</MenuItem>
                                                )
                                            })}
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container direction="row" alignItems="center" spacing={1} >
                                <Grid item xs={2}>
                                    Ward: 
                                </Grid>
                                <Grid item>
                                    <FormControl fullWidth>
                                        <InputLabel>Choose ward</InputLabel>
                                        <Select
                                            value={selectedWard}
                                            label="Choose ward"
                                            onChange={handleWardChange}
                                            sx={{width:250}}
                                        >
                                            {ward.map((value, index)=>{
                                                return(
                                                    <MenuItem value={value.name} key={index}>{value.name}</MenuItem>
                                                )
                                            })} 
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container alignItems="center">
                                <Grid item xs={2}>
                                    Address: &nbsp;
                                </Grid>
                                <Grid item >
                                    <TextField value={selectedAddress} sx={{width:250}} label="Address" variant="outlined" onChange={onAddressHandler}/>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Grid container>
                                <Button variant="contained" style={{backgroundColor: "#d1c286"}} onClick={onBtnSaveHandler}>
                                    Save & Check out
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Modal
                    open={open}
                    onClose={handleClose}
                    >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                        <b>Kindly Give Your Address</b>
                        </Typography>
                    </Box>
                </Modal>
            </ThemeProvider>
    )
}
export default Address;
