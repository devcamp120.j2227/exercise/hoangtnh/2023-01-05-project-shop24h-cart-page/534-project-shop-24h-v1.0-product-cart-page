import { Grid, TableContainer,  Table, TableHead, TableRow, TableCell, TableBody, Paper, Modal, ThemeProvider} from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { checkOrderStatus } from "../../actions/product.action";
import { theme } from "./userProfile";

const StatusOrder = () =>{
    const {user, orderStatus} = useSelector((reduxData)=>
        reduxData.productReducer
    );
    const {accountUser}= useSelector((reduxData)=>
        reduxData.accountReducer
    )
    const dispatch = useDispatch();
    const [uid, setUid] = useState("");
    
    useEffect(()=>{
        const fetchApi = async () => {
            if(accountUser !== null){
                setUid(accountUser.exitUser._id)
            }
            if(user !== null){
                setUid(user.uid)
            }
            try {
                 fetch("http://localhost:8000/api/customers?userId=" + uid)
                    .then(data => data.json())
                    .then( async data => {
                        if (data) {
                            const orderId = data.data.Carts
                            const promises = orderId.map(async value => {
                                const result = await fetch("http://localhost:8000/api/orders/" + value)
                                return result.json()
                            })
                            //map, filter, find,... sẽ trả ra promises 
                            //method Promise.all sẽ chạy đồng thời các promises ở result - tác dụng chạy tất cả cùng 1 lúc tiết kiệm thời gian
                            const orders = await Promise.all(promises)
                            dispatch(checkOrderStatus(orders));
                        }
                    })
            } catch (error) {
                console.log(error.message)
            }
        }
        fetchApi()
    }, [accountUser, user, uid])
    return (
        <Grid container direction="column"
        justifyContent="flex-start" alignItems="flex-start"
        sm={9} style={{backgroundColor:"whiteSmoke", padding: 10, marginBottom: 20}}>
            <Grid item> 
                <h3><b>Status Order</b></h3>
            </Grid>
            <Grid item style={{fontSize: 12, fontStyle:"italic"}}>
                 Your order status
            </Grid>
            <br/>
            <Grid container spacing={1} direction="row"  md={12} sm={12} lg={12} xs={12}>
                <ThemeProvider theme={theme}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: "100%" }}>
                            <TableHead>
                                <TableRow >
                                    <TableCell sx={{color:"#d1c286"}}>Order Code</TableCell>
                                    <TableCell align="right" sx={{color:"#d1c286"}}>Total</TableCell>
                                    <TableCell align="right" sx={{color:"#d1c286"}}>Status</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                            {orderStatus.map((value, index) => {
                                const getPrice =  value.data.Orders;
                                const total = getPrice.reduce((sumary, item)=> sumary+ item.Product.PromotionPrice*item.quantity, 0);
                                return (
                                    <TableRow
                                    key={index}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row">
                                            {value.data._id.substr(value.data._id.length - 6)}
                                        </TableCell>
                                        <TableCell align="right">${total}</TableCell>
                                
                                        <TableCell align="right">{value.data.Status}</TableCell>
                                    </TableRow>
                                )
                             })} 
                            </TableBody>
                        </Table>
                    </TableContainer>
                </ThemeProvider>
            </Grid>
        </Grid>
    )
}
export default StatusOrder;
