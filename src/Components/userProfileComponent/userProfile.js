import { Container, Grid, Input, RadioGroup, FormControlLabel, Radio, Select, MenuItem, InputLabel, FormControl, Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import {  Breadcrumb, BreadcrumbItem  } from "reactstrap";
import { Link } from "react-router-dom";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { ThemeProvider, createTheme } from '@mui/material/styles'; 
import { useEffect, useState } from "react";
import TextField from '@mui/material/TextField';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { getProfileUser, getAddressUser, setPage, setName, setBirthday, setPhone, setGender, fetchApiProfileUser } from "../../actions/product.action";
import Address from "./userAddress";
import EditIcon from '@mui/icons-material/Edit';
import HomeIcon from '@mui/icons-material/Home';
import PaymentIcon from '@mui/icons-material/Payment';
import Payment from "./userPayment";
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import StatusOrder from "./userStatus";
import avatarDefault from "../../assets/default avatar/png-transparent-default-avatar-thumbnail.png"
export const theme = createTheme({
  typography: {
    allVariants: {
      fontFamily: 'Comfortaa',
      textTransform: 'none',
    },
  },
});
const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    
    return data;
}
const UserProfile = () =>{
    const {user, nameLoad, genderLoad, birthdayLoad, phoneLoad, page, loadProfile} = useSelector((reduxData) =>
        reduxData.productReducer
    )
    const {accountUser} = useSelector ((reduxData)=>
        reduxData.accountReducer  
    )
    const [date, setDate] = useState(null);
    const [gender, getGender] = useState("");
    const [name, getName] = useState("");
    const [phoneNumber , getPhoneNumber] = useState("");
    const dispatch = useDispatch();
    const [uid, setUid] = useState("");
    const [email, setEmail] = useState("");
    const [photoURL, setPhotoUrl] = useState("");
    useEffect(()=>{
        if (accountUser === null) {
            setUid(user.uid)
        }
        if (loadProfile === null) {
            setUid(accountUser._id)
        }
    }, [user, accountUser])
    const onProfileClick = () =>{
        dispatch(setPage("profile"))
    }
    const onAddressClick = () =>{
        dispatch(setPage("address"))
    }
    const onPaymentClick = () =>{
        dispatch(setPage("payment"))
    }
    const onStatusClick = () =>{
        dispatch(setPage("status"))
    }
    const onSelectGender = e =>{
        getGender(e.target.value);
    }
    //hàm chuyển lại ngày tháng năm
    function convert(str) {
        if(str === null){
            return ""
        }
        else{
            var date = new Date(str);
            var month = ("0" + (date.getMonth() + 1)).slice(-2);
            var day = ("0" + date.getDate()).slice(-2);
        }
        return [date.getFullYear(), month, day].join("-");
    }
    const userInfor =  {
        userProfile: uid,
        email: email,
        birthday: convert(date),
        gender: gender,
        name: name,
        phone: phoneNumber 
    }
    console.log(userInfor)

    useEffect(() => {
        if (accountUser !== null) {
            setUid(accountUser.exitUser._id)
            setEmail(accountUser.exitUser.Email);
            setPhotoUrl(avatarDefault)
            getName(accountUser.exitUser.Username)
            fetchApi("http://localhost:8000/api/customers?userId=" + accountUser.exitUser._id)
                .then((data) => {
                    dispatch(getProfileUser(data.data));
                })
                .catch((error) => {
                    console.log(error.message)
                });
            }
        if (user !== null) {
            setUid(user.uid)
            setEmail(user.email)
            setPhotoUrl(user.photoURL)
            getName(user.displayName)
            fetchApi("http://localhost:8000/api/customers?userId=" + user.uid)
                .then((data) => {
                    dispatch(getProfileUser(data.data));
                })
                .catch((error) => {
                    console.log(error.message)
                });
            }
        fetchApi("https://provinces.open-api.vn/api/?depth=3")
                .then((data) => {
                    dispatch(getAddressUser(data))
                })
                .catch((error) => {
                    console.log(error.message)
                });

    }, [user, accountUser, uid]);

    const onBtnSaveHandler =() =>{
        dispatch(fetchApiProfileUser(userInfor));
    }
    return (
        <Container > 
            <Breadcrumb>
                <BreadcrumbItem > 
                    <Link to="/" style={{textDecoration:"none"}}>Home</Link>
                </BreadcrumbItem>
                {page === "payment" || page === "address" ?
                <BreadcrumbItem>
                    <Link to="/Payment" style={{textDecoration:"none"}}>Payment</Link>
                </BreadcrumbItem>
                :
                <BreadcrumbItem>
                    <Link to="/User" style={{textDecoration:"none"}}>Profile</Link>
                </BreadcrumbItem>}
                
            </Breadcrumb>
            <Grid container mt={5} direction="row"
                    alignItems="flex-start" md={12} sm={12} lg={12} xs={12}>
                <Grid container direction="column"
                    justifyContent="flex-start"
                    md={3} sm={3} lg={3} xs={3} style={{padding:10}} mb={50}>
                        <Grid container direction="row" alignItems="center" md={12} sm={12} lg={12} xs={12}>
                            <img src={photoURL} style={{borderRadius:"50%", width:"30px"}}/> &nbsp;
                            <p style={{margin: 0}}>{nameLoad !==""? nameLoad: name}</p>
                        </Grid>
                        <br/>
                        
                        {page ==="payment" || page ==="address" ? 
                        <>
                        <Grid item onClick= {onAddressClick} type="button">
                            <HomeIcon fontSize="small"/> Address
                        </Grid>
                        <Grid item onClick= {onPaymentClick} type="button">
                            <PaymentIcon fontSize="small"/> Payment
                        </Grid>
                        </>
                        :
                        <>
                        <Grid item onClick={onProfileClick} type="button"> 
                        {<AccountCircleIcon fontSize="small"/>} Profile
                        </Grid>
                         <Grid item onClick= {onStatusClick} type="button">
                         <LocalShippingIcon fontSize="small"/> Status 
                        </Grid></>
                        }
                </Grid>
                {page ==="profile"?
                <Grid container direction="column"
                    justifyContent="flex-start" alignItems="flex-start"
                    md={9} sm={9} lg={9} xs={9} style={{backgroundColor:"whiteSmoke", padding: 10}}>
                    <Grid item> 
                        <h3><b>My Profile</b></h3>
                    </Grid>
                    <Grid item style={{fontSize: 12, fontStyle:"italic"}}>
                        Manage profile information for account security
                    </Grid>
                    <br/>
                    <ThemeProvider theme={theme} >
                        <Grid container spacing={2} direction="column"  md={12} sm={12} lg={12} xs={12}>
                            <Grid item style={{ alignItems:"center", display:"flex", direction:"row"}}>
                                Name: &nbsp;
                                {nameLoad === "" || nameLoad === undefined ?<TextField value={name} onChange={(value) => getName(value.target.value)} sx={{width:"50%"}}/>
                                : <>
                                    <b>{nameLoad}</b> &nbsp; 
                                        <EditIcon type="button" style={{fontSize:"medium",color:"blue",marginTop:"-15"}} onClick={()=>dispatch(setName(""))}/>
                                        <p style={{fontSize:"10px", color:"blue", marginBottom:0, marginTop:"-15px"}}>edit</p>
                                </>
                                }
                            </Grid>
                            <Grid item>
                                Email: <b>{email}</b>
                            </Grid>
                            <Grid item >
                                <Grid container direction="row" alignItems="center" spacing={1}>
                                    <Grid item>
                                        Gender: 
                                    </Grid>
                                    <Grid item style={{ alignItems:"center", display:"flex", direction:"row"}}>
                                        {genderLoad === "" || genderLoad === undefined ?
                                        <ThemeProvider theme={theme}>
                                            <RadioGroup row>
                                                <FormControlLabel value="female" control={<Radio />} label="Female" onChange={(e)=>onSelectGender(e)} />
                                                <FormControlLabel value="male" control={<Radio />} label="Male" onChange={(e)=>onSelectGender(e)} />
                                                <FormControlLabel value="other" control={<Radio />} label="Other" onChange={(e)=>onSelectGender(e)} />
                                            </RadioGroup>
                                        </ThemeProvider>
                                        : <>
                                            <b>{genderLoad}</b> &nbsp; 
                                            <EditIcon style={{fontSize:"medium",color:"blue",marginTop:"-15"}} type="button" onClick={()=>dispatch(setGender(""))}/>
                                            <p style={{fontSize:"10px", color:"blue", marginBottom:0, marginTop:"-15px"}}>edit</p>
                                          </>
                                        }
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item style={{ alignItems:"center", display:"flex", direction:"row"}}>
                                Phone number: &nbsp;
                                {phoneLoad === ""|| phoneLoad === undefined  ? 
                                <Input type="number" value={phoneNumber} onChange={(value) => getPhoneNumber(value.target.value)} sx={{width:"50%"}}/>
                                :<>
                                    <b>{phoneLoad}</b>
                                    &nbsp; 
                                    <EditIcon style={{fontSize:"medium",color:"blue",marginTop:"-15"}} type="button" onClick={()=> dispatch(setPhone(""))}/>
                                    <p style={{fontSize:"10px", color:"blue", marginBottom:0, marginTop:"-15px"}}>edit</p>
                                </> }
                            </Grid>
                            <Grid item>
                                <Grid container direction="row" alignItems="center" spacing={1}>
                                    <Grid item>
                                        Birthday: &nbsp;
                                    </Grid>
                                    <Grid item style={{ alignItems:"center", display:"flex", direction:"row"}}>
                                        {birthdayLoad === "" || birthdayLoad === undefined? 
                                        <ThemeProvider theme={theme}>
                                            <LocalizationProvider dateAdapter={AdapterDayjs}>
                                                <DatePicker
                                                    label="Birthday"
                                                    value={date}
                                                    onChange={(newDate) => {
                                                    setDate(newDate);
                                                    }}
                                                    renderInput={(params) => <TextField {...params} sx={{width:"80%"}}/>}
                                                />
                                            </LocalizationProvider>
                                        </ThemeProvider> :
                                        <>
                                            <b>{birthdayLoad}</b> 
                                            &nbsp; 
                                            <EditIcon type="button" style={{fontSize:"medium",color:"blue",marginTop:"-15"}} onClick={()=>dispatch(setBirthday(""))}/>
                                            <p style={{fontSize:"10px", color:"blue", marginBottom:0, marginTop:"-15px"}}>edit</p>
                                        </>
                                        }
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Grid container>
                                    <Button variant="contained" style={{backgroundColor: "#d1c286"}} onClick={onBtnSaveHandler}>
                                        Save
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </ThemeProvider>
                </Grid> : 
                    page ==="address" ?
                        <Address/>
                    :page === "payment"?
                        <Payment/>
                    :page ==="status"?
                        <StatusOrder/>
                    :null}
            </Grid>
        </Container>
    )
}
export default UserProfile;
