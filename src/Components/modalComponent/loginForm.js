import { useState, useEffect } from 'react';
import { Button, Row, Col, Input, FormGroup, Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import '@fortawesome/fontawesome-free/css/all.min.css';

import auth from '../../firebase';
import { GoogleAuthProvider, signInWithPopup, signOut, onAuthStateChanged } from 'firebase/auth';
import { useDispatch } from 'react-redux';
import { loginUserInformation } from '../../actions/product.action';
import { Link, useNavigate } from 'react-router-dom';
import CustomizedSnackbars from '../adminPageComponent/CRUDmodalComponents/toast';
import { loginAccount, openToast } from '../../actions/account.action';

const provider = new GoogleAuthProvider();
function LoginForm() {
    const [open, setOpen] = useState(true);
    const navigate = useNavigate();
    //login gg
    const [user, setUser] = useState(null);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const onLogInButton = () =>{
        signInWithPopup(auth, provider)
        .then((result) =>{
            setUser(result.user);
            navigate("/Cart") //đã test xong chuyển hướng thành công
        })
        .catch((error)=>{
            console.error(error)
        });
        setOpen(!open)
    }
    const dispatch = useDispatch();

    //giữ lại giá trị user khi reload trang
    useEffect(() => {
        onAuthStateChanged(auth, (result) => {
            setUser(result);
            dispatch(loginUserInformation(result));
        })
        }, [user])
    const onLoginHandler =()=>{
        const account = {
            username: username,
            password: password
        }
        if(username ===""){
            dispatch(openToast({open: true,
                message: "User name is required",
                type: "error"}))
        }
        if(password ===""){
            dispatch(openToast({open: true,
                message: "Password is required",
                type: "error"}))
        }
        else{
            dispatch(loginAccount(account, ()=> navigate("/")))
        }
    }
  return (
    <>  
    <CustomizedSnackbars/>
        <Card>
            <CardHeader style={{ textAlign:"center"}}><h4>Sign In For Check Your Cart</h4>
            </CardHeader>
                <CardBody className="d-flex justify-content-center"  style={{margin:"3%"}}>
                    <Row >
                        <Col className='d-flex justify-content-center' sm={12}>
                            <Button color='danger' style={{ width:"80%",borderRadius:"50px"}}
                                onClick={onLogInButton}>
                            <i className="fa-brands fa-google"/> Sign In with <b>Google</b> 
                            </Button>
                        </Col>
                        <Col className='d-flex justify-content-center' sm={12}>
                        <div><hr style={{width:"150px",border:"1px solid black"}}/></div>
                        </Col>
                        <Col sm={12} className='d-flex flex-column align-items-center'>
                            <FormGroup style={{width:"80%"}} sm={12}>
                                <Input
                                name="username"
                                placeholder="Username"
                                style={{borderRadius:"50px"}}
                                onChange={(e)=> setUsername(e.target.value)}
                                />
                            </FormGroup>
                            <FormGroup style={{width:"80%"}}>
                                <Input
                                name="password"
                                placeholder="Password"
                                type="password"
                                style={{borderRadius:"50px"}}
                                onChange={(e)=> setPassword(e.target.value)}
                                />
                            </FormGroup>
                            <Button color='success' style={{ width:"80%",borderRadius:"50px"}}
                            onClick={onLoginHandler}>
                                Sign In  
                            </Button>
                        </Col>
                    </Row>
                </CardBody>
                <CardFooter>
                    <Col className='text-center'>
                        <p> Don't have an account?  </p>
                        <Link to="/Account"  style={{color:"green"}}>Sign up here</Link>
                    </Col>
                </CardFooter>
      </Card>
    </> 
  );
}

export default LoginForm;