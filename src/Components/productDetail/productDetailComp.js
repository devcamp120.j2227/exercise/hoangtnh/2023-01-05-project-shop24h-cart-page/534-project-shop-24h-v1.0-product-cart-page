import { Grid, Button } from "@mui/material";
import { Container, Breadcrumb, BreadcrumbItem  } from "reactstrap";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addProductToCartHandler, getProductInfor, quantityButtonHandler } from "../../actions/product.action";
import styled from "styled-components";
import { ellipsis } from "polished";
import '@fortawesome/fontawesome-free/css/all.min.css';
import { Link, useNavigate, useParams } from "react-router-dom";
import InfoIcon from '@mui/icons-material/Info';
import CustomizedSnackbars from "../adminPageComponent/CRUDmodalComponents/toast";
import { openToast } from "../../actions/account.action";

const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    
    return data;
}
const ShowMoreText = styled.button`
        cursor: pointer;
        font-size: 17px;
        margin-bottom: 30px;
        background-color: #d1c286;
        color: black;
        border: 0px;
        padding: 10px 30px;
        border-radius: 10px;
        `;
const DescriptionText = styled.div`
        font-size: 17px;
        margin-top: 20px;
        margin-bottom: 20px;
        ${({ showMore }) => showMore && ellipsis(undefined, 2)}
        `;
const ProductDetailsComp = () =>{
    const { quantity, cartBag, toastAddCart} =  useSelector((reduxData)=>
        reduxData.productReducer
    );
    const [Product, showProduct] = useState([]);
    const [productDetail, getProduct] = useState({});
    const dispatch = useDispatch();
    const navigate = useNavigate();
    //hàm xử lý nút tăng sản phẩm
    const onBtnPlus = () =>{
        dispatch(quantityButtonHandler(quantity + 1));
    };
    //hàm xử lý nút giảm sản phẩm
    const onBtnMinus = () =>{
        dispatch(quantityButtonHandler(quantity - 1))
    };
    const saveData = JSON.parse(localStorage.getItem("item","[]")) || [];
    const {id}=  useParams();

    useEffect(() => {
        fetchApi("http://localhost:8000/api/products/"+ id)
            .then((data) =>{
                getProduct(data.data)
                fetchApi("http://localhost:8000/api/products/description?DescriptionProduct="+ `${data.data.Description}`)
                    .then((data) =>{
                        showProduct(data.Product)
                    })
                    .catch((error)=>{
                        console.log(error.message)
                    });
            })
            .catch((error)=>{
                console.log(error.message)
            });
    }, [id]);
    const onBtnAddToCart = () =>{
        const ProductAdd = {Product: productDetail, quantity: quantity};
        //lưu dữ liệu sản phẩm đã chọn vào local storage
        if(ProductAdd.quantity > 0){
            saveData.push(ProductAdd);
            //lấy ra toàn bộ id để kiểm tra có trùng hay không
            const ids = saveData.map(value => value.Product._id);
            //lọc những id trùng sau đó giữ lại 1 cái duy nhất
            const filtered = saveData.filter(({Product}, index) => !ids.includes(Product._id, index + 1));
            //đẩy dữ liệu các sản phẩm đã lọc được lên local storage
            localStorage.setItem("item",JSON.stringify(filtered));
            dispatch(addProductToCartHandler(filtered));
            dispatch(openToast({
                open: true,
                message: "Add Product to cart bag success",
                type: "success"
                }) )
        }
    };
    //rút gọn hoặc hiển thị thêm thông tin sản phẩm
    const [isShowMore, setIsShowMore] = useState(true);
    const toggleReadMore = () => setIsShowMore((show) => !show);
    const onProductClick = (value) =>{
        dispatch(getProductInfor(value))
    }
    return (
        <Container>
             <CustomizedSnackbars/>
            <Breadcrumb md={12} sm={12} lg={12} xs={12}>
                <BreadcrumbItem > 
                    <Link to="/" style={{textDecoration:"none"}}>Home</Link>
                </BreadcrumbItem>
                <BreadcrumbItem>
                    <Link to="/Categories" style={{textDecoration:"none"}}>Categories</Link>
                </BreadcrumbItem>
                <BreadcrumbItem>
                    <Link to="/Categories/More" style={{textDecoration:"none"}}>Products</Link>
                </BreadcrumbItem>
                <BreadcrumbItem active>
                    <Link to="/Categories/More/Detail">Product Details</Link>
                </BreadcrumbItem>
            </Breadcrumb>
            <Grid container direction="row" md={12} sm={12} lg={12} xs={12}
                    justifyContent="center"
                    alignItems="center">
                <Grid item md={6} sm={6} lg={6} xs={6} >
                    <img src={productDetail.ImageUrl} alt="product" style={{maxWidth: "100%"}}/>
                </Grid>
                <Grid item md={6} sm={6} lg={6} xs={6}  style={{padding:"10px", marginTop:"5%"}}>
                    <h2>{productDetail.Name}</h2>
                    <p>Type: <b>{productDetail.Type}</b></p>
                    <p>Description: <b>{productDetail.Description}</b></p>
                    <h4 style={{color:"#d1c286"}}>${productDetail.PromotionPrice}</h4>
                    <Grid container direction="row" alignItems="center">
                        <button style={{margin:"5px",padding:"2px 11px", borderRadius:"50%", border:"0px", backgroundColor:"#d1c286", color:"white"}}
                               onClick={() => onBtnMinus()}>-</button>
                        <p style={{margin:"5px"}}>{quantity}</p>
                        <button style={{margin:"5px",padding:"2px 9px", borderRadius:"50%", border:"0px", backgroundColor:"#d1c286", color:"white"}}
                                onClick={() => onBtnPlus()}>+</button>
                    </Grid>
                    <Button onClick ={() =>onBtnAddToCart()} style={{backgroundColor:"#d1c286", color:"white", marginTop:"10px", padding:"10px"}}> 
                        Add to Cart &nbsp;<i type="button" className="fa-solid fa-cart-shopping"/>
                    </Button>
                </Grid>
                <Grid item xs ={12} md = {12}>
                    <h4>Information</h4>
                    <DescriptionText showMore={isShowMore}>
                        <p>{productDetail.ProductDescription}</p>
                        <p>Old Price: <b>${productDetail.BuyPrice}</b></p>
                        <p>Ship Fee: <b>{productDetail.DeliveryFee === null ||productDetail.DeliveryFee === undefined? <b>Free</b> : productDetail.DeliveryFee}</b></p>
                        <p>Delivery: <b>{productDetail.DeliverTime === null || productDetail.DeliverTime === undefined? <b>Within 7 days from the date of order</b> :productDetail.DeliverTime}</b></p>
                        <p>Warranty: <b>{productDetail.Warranty === null || productDetail.Warranty === undefined? <b>36 months</b>:productDetail.Warranty}</b></p>
                    </DescriptionText>
                    <Grid container direction="row"
                            justifyContent="center"
                            alignItems="center">
                        <ShowMoreText  onClick={toggleReadMore}>
                            {isShowMore ? "View All" : "View Less"}
                        </ShowMoreText>
                    </Grid>
                </Grid>
                <Grid item xs ={12} md = {12} mb={5}>
                    <h4>Related Products</h4>
                    <Grid container spacing={2}  direction="row"
                    justifyContent="space-around"
                    alignItems="center">
                        {Product.map((value, index) =>{
                            return(
                                <>
                                <Grid item xs={4} md={4} key={index} className="img-Box">
                                    <img className="related-img"
                                    src={value.ImageUrl} alt="product" style={{maxWidth:"70%",display: "block", marginLeft: "auto", marginRight: "auto"}}
                                     onClick={()=> {onProductClick(value); navigate("/Categories/More/Details/"+`${value._id}`); window.scrollTo({ top: 0, behavior: 'smooth' })}}/>
                                    <Grid className="iconColor">
                                     <div className="iconDiv">
                                        <InfoIcon  type="button" onClick={()=> {onProductClick(value); navigate("/Categories/More/Details/"+`${value._id}`);window.scrollTo({ top: 0, behavior: 'smooth' })}}/>
                                     </div>
                                     </Grid>

                                </Grid>
                                </>
                            )
                        })}
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    )
}
export default ProductDetailsComp;
