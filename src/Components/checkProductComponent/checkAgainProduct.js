import { Container, Grid, TableContainer, Table, TableRow, TableCell, TableBody, Paper, TableHead,Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import ClearIcon from '@mui/icons-material/Clear';
import { addProductToCartHandler, onBtnMinusHandler, onBtnPlusHandler, onBtnRemoveProduct, setPage } from "../../actions/product.action";
import { useNavigate } from "react-router-dom";
import { ThemeProvider, createTheme } from '@mui/material/styles'; 
import avatarDefault from "../../assets/default avatar/png-transparent-default-avatar-thumbnail.png"
import { useState } from "react";
import { useEffect } from "react";

const theme = createTheme({
  typography: {
    allVariants: {
      fontFamily: 'Comfortaa',
      textTransform: 'none',
      fontSize: 16,
    },
  },
});
const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    
    return data;
}
const CheckProductAndPayment = () =>{
    const {cartBag, user} = useSelector((reduxData) =>
        reduxData.productReducer
    );
    const {accountUser} =  useSelector((reduxData)=>
        reduxData.accountReducer
    );
    const [uid, setUid] = useState("");
    const [email, setEmail] = useState("");
    const [photoURL, setPhotoUrl] = useState("");
    const [displayName, setDisplayName] = useState("")
    useEffect(()=>{
        if(accountUser !== null){
            console.log(accountUser)
            setUid(accountUser.exitUser._id)
            setEmail(accountUser.exitUser.Email);
            setPhotoUrl(avatarDefault)
            setDisplayName(accountUser.exitUser.Username)
        }
        if(user!== null){
            setUid(user.uid)
            setEmail(user.email)
            setPhotoUrl(user.photoURL)
            setDisplayName(user.displayName)
        }
    }, [accountUser, user, uid])
    const navigate = useNavigate();
    const dispatch = useDispatch();
        //hàm xử lý nút tăng sản phẩm
        const onBtnPlus = (e) =>{
            dispatch(onBtnPlusHandler(e.Product._id));
        };
        //hàm xử lý nút giảm sản phẩm
        const onBtnMinus = (e) =>{
            dispatch(onBtnMinusHandler(e.Product._id));
        };
        //hàm xử lý nút xóa sản phẩm
        const onBtnDelProduct = (e) =>{
            dispatch(onBtnRemoveProduct(e.Product._id));
        }
        const total = cartBag.reduce((sumary, item)=> sumary+ item.Product.PromotionPrice*item.quantity, 0);

        const onBtnCheckOut = () =>{
                var requestOptions = {
                    method: 'PUT',
                    headers: {"Content-Type": "application/json"},
                    body: JSON.stringify({
                        Orders: cartBag
                    }),
                    redirect: 'follow'
                  };
                fetchApi("http://localhost:8000/api/customers?userId="+ uid, requestOptions)
                    .then((data)=>{
                        if(data.data === null){
                            const requestOptions = {
                                method: 'POST',
                                headers: {
                                    'Content-type': 'application/json; charset=UTF-8'
                                },
                                body: JSON.stringify({
                                    UserId: uid,
                                    Name: "",
                                    Email: email,
                                    Birthday: "",
                                    Phone: "",
                                    Gender: "",
                                    Address: "",
                                    Orders: cartBag
                                    }),
                                redirect: 'follow'
                                };
                            fetchApi("http://localhost:8000/api/customers", requestOptions)
                                .then((data)=>{
                                    dispatch(addProductToCartHandler(data.data.Orders))
                                    navigate("/Payment");
                                    dispatch(setPage("address"));
                                })
                                .catch((error)=>{
                                    console.log(error.message)
                                })
                        }
                        if(total > 0){
                            dispatch(addProductToCartHandler(data.data.Orders))
                            navigate("/Payment");
                            dispatch(setPage("address"));
                        }
                    })
                    .catch((error)=>{
                        console.log(error.message)
                    })
        }
    return(
        <Container md={12} sm={12} lg={12} xs={12}>
            <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                    spacing={2}
                    margin={5}>
                <Grid item>
                    <b>COSY-HOME</b>
                </Grid>
                <Grid item>
                    |
                </Grid>
                <Grid item>
                    SHOPPING CART
                </Grid>
            </Grid>
            {user || accountUser?
            <ThemeProvider theme={theme}>
                <TableContainer component={Paper} className="mb-4">
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left"><b>Products</b></TableCell>
                                <TableCell align="left" ><b>Price</b></TableCell>
                                <TableCell align="left" ><b>Quantity</b></TableCell>
                                <TableCell align="left" ><b>Total</b></TableCell>
                                <TableCell align="left" ></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {cartBag.map((value, index) => (
                            <TableRow
                            key={index}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                            <TableCell component="th" scope="row" align="left"style={{width:"80%"}}><img src={value.Product.ImageUrl} style={{height:"auto", width:"100px"}}/> &nbsp;&nbsp;{value.Product.Name}</TableCell>
                            <TableCell align="left"style={{width:"100%",paddingRight:0  }}>$ {value.Product.PromotionPrice}</TableCell>
                            <TableCell align="left" style={{width:"100%"}}>
                                <Grid container direction="row"
                                    justifyContent="flex-start"
                                    alignItems="center" sx={{width:"100px"}}>
                                        <Grid item>
                                            <button style={{margin:"5px",padding:"2px 11px", borderRadius:"50%", border:"0px", backgroundColor:"#d1c286", color:"white"}}
                                                onClick={() => onBtnMinus(value)}>-</button>
                                        </Grid>
                                        <Grid item>
                                            {value.quantity <= 0? 1:value.quantity}
                                        </Grid>
                                        <Grid item>
                                            <button style={{margin:"5px",padding:"2px 9px", borderRadius:"50%", border:"0px", backgroundColor:"#d1c286", color:"white"}}
                                                onClick={() => onBtnPlus(value)}>+</button>
                                        </Grid>
                                    </Grid>      
                            </TableCell>
                            <TableCell align="left" style={{width:"100%",paddingRight:0}}> $ {value.Product.PromotionPrice * value.quantity}</TableCell>
                            <TableCell align="left"><ClearIcon type="button" onClick= {()=> onBtnDelProduct(value)}/></TableCell>
                            </TableRow>
                        ))} 
                        </TableBody>
                    </Table>
                </TableContainer>
                <Grid>
                    <Button style={{backgroundColor:"gray", color: "white",marginBottom:"10px"}} onClick={()=>navigate("/Categories/More")}> Continue Shopping</Button>
                </Grid>
                <Grid container  style={{backgroundColor:"whiteSmoke", padding:10, width:"50%", margin:"auto", marginBottom:20}}
                        direction="column" md={12} sm={12} lg={12} xs={12}>
                    <Grid item  textAlign="center" marginBottom={5}>
                        <h4>Cash total</h4>
                    </Grid>
                    <Grid container sx={{width:"100%"}}
                        direction="row"
                        marginBottom={5}>
                        <Grid item textAlign="center" xs={6}>
                            <b>Customer:</b>
                        </Grid>
                        <Grid item textAlign="center"xs={6}>
                            <b style={{color:"#d1c286"}}> {displayName}</b>
                        </Grid>
                    </Grid>
                    <Grid container sx={{width:"100%"}}
                        direction="row"
                        marginBottom={5}>
                        <Grid item textAlign="center" xs={6}>
                            <b>Total:</b>
                        </Grid>
                        <Grid item textAlign="center"xs={6}>
                            <b style={{color:"#d1c286"}}>$ {total}</b>
                        </Grid>
                    </Grid>
                    <Grid container
                        direction="row"
                        justifyContent="center" >
                        <Button style={{backgroundColor:"#7fad39", color:"white", padding:"10px"}} onClick={onBtnCheckOut}><b>Process To Check out</b></Button>
                    </Grid>
                </Grid>
                </ThemeProvider>
        : <Grid textAlign="center" margin={"15%"} style={{color:"#d1c286"}}>You need to sign in for completing check out your cart</Grid>}
        
        </Container>
    )
}
export default CheckProductAndPayment;
