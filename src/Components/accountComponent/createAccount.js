import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

import "./style.css";
import { Button, TextField, Grid, InputAdornment, IconButton, InputLabel, Input, FormControl, FormHelperText } from "@mui/material";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { createNewAccount } from '../../actions/account.action';
const CreateAccount = () =>{

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [score, setScore] = useState(0);
  const [showPassword, setShowPassword] = useState(false);
  const [userName, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [errorUsername, setErrorUsername] = useState(true)
  const [errorEmail, setErrorEmail] = useState(true)
  const [errorPassword, setErrorPassword] = useState(true)
  const [errorConfirmPassword, setErrorConfirmPassword] = useState(true)
    //khai báo user infor
    const user ={
      userName: userName,
      email: email,
      password: password,
      confirmPassword: confirmPassword
    }
    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event) => {
      event.preventDefault();
    };
    const PasswordStr = props => {
        var strColor = "";
        var strWidth = "";
      
        if(props.score <= 4) {
            strColor = 'red';
            strWidth = '20%';
        }
          if(props.score >= 5) {
            strColor = 'orange';
            strWidth = '40%';
        }
          if(props.score >= 6) {
            strColor = 'yellow';
            strWidth = '60%';
        } 
          if(props.score >= 7) {
            strColor = '#5cff47';
            strWidth = '80%';
        } 
          if(props.score >= 8) {
            strColor = 'green';
            strWidth = '100%';
        } 
        var style = { backgroundColor: strColor, height: '5px', width: strWidth, transition: 'all 300ms ease-in-out' }
        return (
        <div>
          <p className="pwStrWeak">weak</p>
          <p className="pwStrStrong">strong</p>
          <div style={style} />
        </div> 
        );
      }
      const onSubmit =()=>{
        console.log(user)
        if(userName ===""){
          setErrorUsername(false)
        }else{
          setErrorUsername(true);
        }
        if(email ==="" || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email) === false){
          setErrorEmail(false)
        }
        else{
          setErrorEmail(true)
        }
        if(password.length < 8 || password ===""){
          setErrorPassword(false)
        }
        else{
          setErrorPassword(true)
        }
        if(confirmPassword !== password || confirmPassword ===""){
          setErrorConfirmPassword(false)
        }
        else{
          setErrorConfirmPassword(true);
          dispatch(createNewAccount(user, ()=>navigate("/Login")))
        }
      }
    return (
      <div className="Sign-up-page" >
        <div className="loginBox">
          <h1>Sign Up</h1>

          <FormControl >
            <Grid container direction="column" justifyContent="space-between" height={350}>
            <TextField
                label="email"
                floatingLabelText="email"
                variant="standard"
                type="email"
                value={email}
                onChange={(e)=> setEmail( e.target.value)}
              />
              {errorEmail === false? 
              <FormHelperText style={{color:"red", marginLeft:0}}>Please provide an true email.</FormHelperText>
              : null}
              <TextField
                label="username"
                variant="standard"
                value={user.userName}
                onChange={(e)=> setUsername(e.target.value)}
              />
              {errorUsername === false? 
              <FormHelperText style={{color:"red",marginLeft:0}}>Please provide a user name.</FormHelperText>
              : null}
              
                <FormControl variant="standard" className="pwStrRow">
                  <InputLabel htmlFor="standard-adornment-password">password</InputLabel>
                  <Input
                    onChange={(e) => {setScore(e.target.value.length); setPassword(e.target.value)}}
                    value={password}
                    id="standard-adornment-password"
                    type={showPassword ? 'text' : 'password'}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  {score >= 1 && (
                  <div>
                    <PasswordStr score={score} />
                  </div>
                )}
                {errorPassword === false? 
              <FormHelperText style={{color:"red"}}>Password must have at least 8 characters.</FormHelperText>
              : null}
                </FormControl>
                
              
              <FormControl variant="standard" >
                  <InputLabel htmlFor="standard-adornment-confirm-password">confirm password</InputLabel>
                  <Input
                    value={confirmPassword}
                    onChange={(e)=> setConfirmPassword(e.target.value)}
                    id="standard-adornment-confirm-password"
                    type={showPassword ? 'text' : 'password'}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  {errorConfirmPassword === false? 
                    <FormHelperText style={{color:"red"}}>Password confirmation doesn't match.</FormHelperText>
                    : null}
              </FormControl>
              <Button variant="contained" className="button-sign-up" onClick={onSubmit}>Create New Account</Button>
            </Grid>
          </FormControl>
          <hr/>
          <p>
            Already have an account? <br />
            {window.location.href ==="http://localhost:3000/Administrator/Account"?
            <Link to="/LoginAdmin">Log in here</Link>: 
            <Link to="/Login">Log in here</Link>
            }
            
          </p>
        </div>
      </div>
    )
}

export default CreateAccount;
