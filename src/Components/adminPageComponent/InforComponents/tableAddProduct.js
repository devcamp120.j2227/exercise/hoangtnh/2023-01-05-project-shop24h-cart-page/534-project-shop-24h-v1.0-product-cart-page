import { Grid, Typography, TableContainer, Table, TableRow, TableCell, TableBody, Paper, TableHead, Pagination, Button, TextField, MenuItem } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { filterByProductName, filterProduct, getAllProduct, getCartBag, getCurrentPageProduct, getQuantity } from "../../../actions/account.action";
import { useState } from "react";

const TableAddProduct = () =>{
    const {noPage, currentPageProduct, limitProduct, allProducts, descpitionProduct, productFilter,
             productName} = useSelector((reduxData)=>
            reduxData.accountReducer
    )
    const dispatch = useDispatch();
    const [quantity, setQuantity] = useState(null)
    const onChangePagination=(event, value)=>{
        if(productFilter ==="All"){
            dispatch(getCurrentPageProduct(value));
            dispatch(getAllProduct(value, limitProduct));
        }
        else{
            dispatch(filterProduct(productFilter, value, limitProduct));
            dispatch(getCurrentPageProduct(value));
        }
    }
    useEffect(()=>{
        if(productFilter ==="All" || productName ===""){
            dispatch(getAllProduct(currentPageProduct, limitProduct))
        }
    }, []);
    
    //filter by description
    const onProductFilter =(e) =>{
        dispatch(filterProduct(e.target.value, 1, limitProduct));
    }
    const onAddProductToCart = (value)=>{
        if(quantity > 0){
            dispatch(getCartBag({Product: value, quantity: quantity}));
        }
        setQuantity(null)
    }
    const onChangeQuantity = (e) =>{
        setQuantity(e)
        if(e < 0){
            setQuantity(null)
        }
    }
    return (
        <>
            <Grid container direction="row"
                alignItems="center" spacing={2} style={{ marginBottom: "10px" }}>
                <Grid item>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="center">
                        <Grid item >
                            <TextField
                                label="Filter product name"
                                value={productName}
                                variant="standard"
                                style={{ width: "200px"}}
                                onChange={(e) => dispatch(filterByProductName(e.target.value.toUpperCase()))}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="center">
                        <Grid item >
                            <TextField
                                select
                                label="Filter description"
                                value={productFilter}
                                variant="standard"
                                style={{ width: "200px" }}
                                onChange={(e) => onProductFilter(e)}
                            >
                                {descpitionProduct.map((option) => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <TableContainer component={Paper}  className="form-admin-page">
                <Table aria-label="collapsible table" md={12} sm={12} lg={12} xs={12} 
                    sx={{
                        "& .MuiTableRow-root:hover": {
                        backgroundColor: "whiteSmoke",
                        }
                    }}
                >
                    <TableHead>
                        <TableRow style={{backgroundColor:"whiteSmoke"}}>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Id</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Product</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Type</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Description</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Buy Price</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Promotion Price</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Quantity</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Action</b></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {allProducts.map((value, index) => {
                            return (
                                <TableRow
                                    key={index}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
                                        <TableCell align="left"><b>#{value._id.substr(value._id.length - 6)}</b></TableCell>
                                        <TableCell align="left">
                                            <Grid container direction="row" justifyContent="flex-start"
                                                alignItems="center">
                                                <img src={value.ImageUrl} style={{ width: "50px"}} />
                                                <Typography>{value.Name}</Typography>
                                            </Grid>
                                        </TableCell>
                                        <TableCell align="left">{value.Type}</TableCell>
                                        <TableCell align="left">{value.Description}</TableCell>
                                        <TableCell align="left">$ {value.BuyPrice}</TableCell>
                                        <TableCell align="left">$ {value.PromotionPrice}</TableCell>
                                        <TableCell align="left">
                                            <TextField style={{ width: "80px" }} variant="outlined" type="number" value={quantity === null? 0: quantity.index} onChange={(e) => onChangeQuantity(e.target.value)}/>
                                        </TableCell>
                                        <TableCell align="left">
                                            <Grid container direction="row" justifyContent="flex-start"
                                                alignItems="center">
                                                <Button  style={{color:"#d1c286"}} onClick={()=>{onAddProductToCart(value)}}>
                                                        <b> ADD</b>
                                                </Button>
                                            </Grid>
                                        </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <Grid item md={12} sm={12} lg={12} xs={12} mt={5} mb={5}
                container
                direction="row"
                justifyContent="center"
                alignItems="center">
                <Pagination count={noPage} defaultPage={1} onChange={onChangePagination}/>
            </Grid>
        </>
    )
}
export default TableAddProduct;