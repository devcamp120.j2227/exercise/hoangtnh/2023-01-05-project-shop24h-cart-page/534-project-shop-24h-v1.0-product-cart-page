import { useDispatch, useSelector } from "react-redux"
import { Dialog, Typography, Grid, DialogTitle, DialogContent, DialogActions, Button } from "@mui/material";
import { getStatusDeleteImg, openToast } from "../../../actions/account.action";
import CustomizedSnackbars from "./toast";
import { storage } from "../../../firebase";
import { ref, deleteObject } from "firebase/storage";
export const DeleteModalImage = () =>{
    const dispatch = useDispatch();
    const {imgStatus, toast, imgUrl} = useSelector((reduxData)=>
        reduxData.accountReducer
    )
    const onBtnDeleteHandler = () =>{
        //lấy data trên firebase bằng url
        // Create a reference to the file to delete
        const httpsReference = ref(storage, imgUrl);
    
        // Delete the file
        deleteObject(httpsReference).then(() => {
            // File deleted successfully
            dispatch(getStatusDeleteImg(false))
            dispatch(openToast({
                open: true,
                message: "Delete image successfull",
                type: "warning"
            }))
        }).catch((error) => {

        });
    }
    const handleClose = () =>{
        dispatch(getStatusDeleteImg(false));
    }
    return(
        <>
            <Dialog open= {imgStatus} onClose={handleClose} scroll= "paper">  
                    <DialogTitle>
                        <Typography textAlign="center" variant="h6" component="h2">
                            DELETE IMAGE
                        </Typography>
                    </DialogTitle>
                    <DialogContent dividers='paper'>
                        <Typography sx ={{mt:2}}>
                            <Grid container spacing={2}>
                                <Grid item >
                                    <p>Confirm to DELETE this image</p>
                                </Grid>
                            </Grid>
                        </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button  onClick={handleClose} variant="outlined">Close</Button>
                        <Button onClick={onBtnDeleteHandler} variant="contained" style={{backgroundColor:"#d41616"}}>Delete Image</Button>
                    </DialogActions>
            </Dialog>
            {toast.open? <CustomizedSnackbars/>: null}

        </>
    )
}