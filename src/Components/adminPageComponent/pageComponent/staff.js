import { ArrowRight } from "@mui/icons-material";
import { Grid, Typography, TableContainer, Table, TableRow, TableCell, TableBody, Paper, TableHead, Pagination, Button, TextField, MenuItem } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { filterProduct, getAllProduct, getCurrentPageProduct } from "../../../actions/account.action";
import DeleteIcon from '@mui/icons-material/Delete';
import ModeEditIcon from '@mui/icons-material/ModeEdit';
import { Link } from "react-router-dom";
import AddIcon from '@mui/icons-material/Add';

const ManagerProduct = () =>{
    const {noPage, currentPageProduct, limitProduct, allProducts, descpitionProduct, productFilter} = useSelector((reduxData)=>
            reduxData.accountReducer
    )
    const dispatch = useDispatch();

    const onChangePagination=(event, value)=>{
        if(productFilter ==="All"){
            dispatch(getCurrentPageProduct(value));
            dispatch(getAllProduct(value, limitProduct));
        }
        else{
            dispatch(filterProduct(productFilter, value, limitProduct));
            dispatch(getCurrentPageProduct(value));
        }
    }
    useEffect(()=>{
        if(productFilter ==="All"){
            dispatch(getAllProduct(currentPageProduct, limitProduct))
        }
    }, [])
    const onProductFilter =(e) =>{
        dispatch(filterProduct(e.target.value, 1, limitProduct));
    }
    
    return (
        <>
            <Typography style={{color:"#d1c286"}}>
                <b><ArrowRight/>PRODUCT MANAGER</b>
            </Typography>
            <Grid container direction="row"
                alignItems="center" spacing={2} style={{ marginBottom: "10px", marginTop:"20px" }}>
                <Grid item xs={6}>
                    <Grid container direction="row"
                        justifyContent="flex-start"
                        alignItems="center">
                        <Grid item>
                            <Link style={{textDecoration:"none"}} to="/Administrator/CreateProduct">
                                <Button style={{backgroundColor:"#d1c286", color:"white", fontSize:"16px"}}><AddIcon/>Create Product</Button>
                            </Link>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={6}>
                    <Grid container direction="row"
                        justifyContent="flex-end"
                        alignItems="center">
                        <Grid item >
                            <TextField
                                select
                                label="Find product description"
                                value={productFilter}
                                variant="standard"
                                style={{ width: "200px" }}
                                onChange={(e) => onProductFilter(e)}
                            >
                                {descpitionProduct.map((option) => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <TableContainer component={Paper}  className="form-admin-page">
                <Table aria-label="collapsible table" md={12} sm={12} lg={12} xs={12} 
                    sx={{
                        "& .MuiTableRow-root:hover": {
                        backgroundColor: "whiteSmoke",
                        }
                    }}
                >
                    <TableHead>
                        <TableRow style={{backgroundColor:"whiteSmoke"}}>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Id</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Product</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Type</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Description</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Buy Price</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Promotion Price</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Product Description</b></TableCell>
                            <TableCell align="left" style={{color:"#d1c286"}}><b>Action</b></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {allProducts.map((value, index) => {
                            return (
                                <TableRow
                                    key={index}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }} >
                                        <TableCell align="left"><b>#{value._id.substr(value._id.length - 6)}</b></TableCell>
                                        <TableCell align="left">
                                            <Grid container direction="row" justifyContent="flex-start"
                                                alignItems="center">
                                                <img src={value.ImageUrl} style={{ width: "50px"}} />
                                                <Typography>{value.Name}</Typography>
                                            </Grid>
                                        </TableCell>
                                        <TableCell align="left">{value.Type}</TableCell>
                                        <TableCell align="left">{value.Description}</TableCell>
                                        <TableCell align="left">$ {value.BuyPrice}</TableCell>
                                        <TableCell align="left">$ {value.PromotionPrice}</TableCell>
                                        <TableCell align="left">{value.ProductDescription.slice(0, 80) + ' ...'}</TableCell>
                                        <TableCell align="left">
                                            <Grid container direction="row" justifyContent="flex-start"
                                                alignItems="center">
                                                    <Link to={"/Administrator/Staff/"+ value._id}>
                                                        <Button>
                                                            <ModeEditIcon style={{color:"#d1c286"}}/>
                                                        </Button>
                                                    </Link>
                                                <Button color="error">
                                                    <DeleteIcon/>
                                                </Button>
                                            </Grid>
                                        </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            <Grid item md={12} sm={12} lg={12} xs={12} mt={5} mb={5}
                container
                direction="row"
                justifyContent="center"
                alignItems="center">
                <Pagination count={noPage} defaultPage={1} onChange={onChangePagination}/>
            </Grid>
        </>
    )
}
export default ManagerProduct;