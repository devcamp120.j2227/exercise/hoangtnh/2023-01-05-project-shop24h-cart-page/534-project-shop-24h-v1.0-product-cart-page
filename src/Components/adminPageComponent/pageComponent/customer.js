import { TableContainer, Table, TableRow, TableCell, TableBody, Paper, TableHead, Pagination, Grid} from "@mui/material";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllCustomers } from "../../../actions/account.action";
import InfoIcon from '@mui/icons-material/Info';
import { Link, useNavigate } from "react-router-dom";

const CustomersInfo = () =>{
    const {customer} = useSelector((reduxData)=>
        reduxData.accountReducer
    )
    const dispatch = useDispatch()
    useEffect(()=>{
            dispatch(getAllCustomers());
    },[])
    return (
        <>
            <TableContainer component={Paper}  className="form-admin-page">
                <Table aria-label="collapsible table" md={12} sm={12} lg={12} xs={12}>
                    <TableHead>
                    <TableRow style={{backgroundColor:"whiteSmoke"}}>
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Customer</b></TableCell>
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Email</b></TableCell>
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Phone</b></TableCell>
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Total Spent</b></TableCell>
                        <TableCell align="left" style={{color:"#d1c286"}}><b>Detail</b></TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                        {customer.map((row, index) => {
                            return(
                            <TableRow
                            
                            key={index}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    <b>{row.Name}</b>
                                </TableCell>
                                <TableCell align="left"><b>{row.Email}</b></TableCell>
                                <TableCell align="left"><b>{row.Phone}</b></TableCell>
                                <TotalRow value={row} row={row}/>
                                <TableCell align="left">
                                    <Link to={"/Administrator/Customer/" + row.UserId}><InfoIcon style={{color:"#d1c286"}}/>
                                    </Link></TableCell>
                            </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
                {/* <Grid item md={12} sm={12} lg={12} xs={12} mt={5} mb={5}
                        container
                        direction="row"
                        justifyContent="center"
                        alignItems="center">
                    <Pagination count={noPage} defaultPage={currentPage} props={{color:"#d1c286"}} onChange={onChangePagination} onClick={window.scrollTo({ top: 0, behavior: 'smooth' })}/>
                </Grid> */}
            </TableContainer>
        </>
    )
}
function TotalRow({value}) {
    const [total, getTotal] = useState(0);

    useEffect(()=>{
            if(value.Carts.length === 0){
                getTotal(0)
            }
            else{
                try {
                    fetch("http://localhost:8000/api/customers?userId=" + value.UserId)
                       .then(data => data.json())
                       .then( async data => {
                           if (data) {
                               const orderId = data.data.Carts
                               const promises = orderId.map(async value => {
                                   const result = await fetch("http://localhost:8000/api/orders/" + `${value}`)
                                   return result.json()
                               })
                               //map, filter, find,... sẽ trả ra promises 
                               //method Promise.all sẽ chạy đồng thời các promises ở result - tác dụng chạy tất cả cùng 1 lúc tiết kiệm thời gian
                                const orders = await Promise.all(promises);
                                var result = []
                                orders.filter(obj => {
                                const arr = obj.data.Orders;
                                const i = arr.reduce((sumary, item) => sumary + item.Product.PromotionPrice*item.quantity, 0);
                                result.push(i)
                                })
                               getTotal(result.reduce((n, sum) => n + sum, 0))
                           }
                       })
               } catch (error) {
                   console.log(error.message)
               }
            }
     },[ value ])
    return (
        <>
            <TableCell align="left"><b>{total === 0 ? 0: `$ ${total}`}</b></TableCell>
        </>
    )
}
export default CustomersInfo;