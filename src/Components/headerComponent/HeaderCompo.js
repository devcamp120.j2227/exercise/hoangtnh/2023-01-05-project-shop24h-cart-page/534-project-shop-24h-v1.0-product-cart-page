import { Col, Row } from "reactstrap";
import '@fortawesome/fontawesome-free/css/all.min.css';
import { useNavigate } from "react-router-dom";
import LoginModal from "../modalComponent/loginModal";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import ShoppingCart from "../modalComponent/checkCartBag";
import { changeCartStatus } from "../../actions/product.action";
import { Container } from "@mui/material";
const Header = () =>{
    const navigate = useNavigate();
    const onLogoClick = () =>{
        navigate("/");
    }
    const {numberProduct, checkCartStatus} = useSelector((reduxData)=>
    reduxData.productReducer
)
    const onBtnProductClick = () =>{
        navigate("/Categories");
    }
   
    const dispatch = useDispatch();
    const [status, setStatus] = useState(false)
    const onCheckCartHandler = () =>{
        if(numberProduct > 0){
            setStatus(!status)
            dispatch(changeCartStatus(!checkCartStatus))
        }
    }
    return (
        <>
        {checkCartStatus? <ShoppingCart/>:null}
        <div style={{backgroundColor:"#d1c286"}}>
                <Row md={12} sm={12} lg={12} xs={12} style={{padding:"10px", backgroundColor:"#d1c286"}} >
                    <Col md={4} sm={4} lg={4} xs={4} className="d-flex align-items-center justify-content-center">
                        <h3 style={{color:"white"}} onClick={onLogoClick} type="button">Devcamp COSY-HOME</h3>
                    </Col>
                    <Col md={6} sm={6} lg={6} xs={6}  className="d-flex align-items-center justify-content-center" style={{padding:"0px"}}>
                        <Row xs={12} style={{margin:"0px", width:"100%"}}>
                            <Col sm={3} className = "header-content text-center" style={{padding:"5px"}}>
                                <a href="#" onClick={onBtnProductClick}>PRODUCT</a>
                            </Col>
                            <Col sm={3} className = "header-content text-center" style={{padding:"5px"}}>
                                <a href="#">ABOUT</a>
                            </Col>
                            <Col sm={3} className = "header-content text-center" style={{padding:"5px"}}>
                                <a href="#" >SERVICES</a>
                            </Col>
                            <Col sm={3} className = "header-content text-center"style={{padding:"5px"}}>
                                <a href="#" >CONTACT</a>
                            </Col>
                        </Row>
                    </Col>
                    <Col  md={2} sm={2} lg={2} xs={2}  className="d-flex align-items-center justify-content-end">
                        <Col md={8} sm={8} lg={8} xs={8} className="awesome-icon d-flex align-items-center justify-content-end" style={{marginRight:"5px"}}>
                            <p style={{margin:"0px"}}>
                                <i type="button" className="fa-solid fa-cart-shopping " onClick={()=>onCheckCartHandler()}></i>
                                    {numberProduct !== 0?<span className='badge badge-warning' id='lblCartCountHeader'> {numberProduct} </span>:null}
                                Cart</p>
                        </Col>
                        <Col md={4} sm={4} lg={4} xs={4}  className="awesome-icon d-flex align-items-center justify-content-center">
                            <LoginModal/>
                        </Col>
                    </Col>
                </Row> 
        </div>
        </>
    )
}
export default Header;